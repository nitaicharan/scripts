source config.sh

# https://hispagatos.org/post/neomutt-gpg-howto/

PROGRAM="NeoMutt"
CONFIG_FOLDER="neomutt"

echo "-------------> Installing $PROGRAM"
$pminstall gnupg pass neomutt

echo "-------------> Configurando $PROGRAM"
mkdir --parent ~/.config/$CONFIG_FOLDER
ln --symbolic --force $(pwd)/$PROGRAM/* ~/.config/$CONFIG_FOLDER/
