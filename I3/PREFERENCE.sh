source config.sh
echo "-------------> Install I3"
#$pminstall i3
#$pminstall pulseaudio

echo "-------------> Configurando I3"
# Configurando interface I3
mkdir -p ~/.config/i3/
ln -sf $(pwd)/I3/*  ~/.config/i3/
i3-msg reload > /dev/null
