source config.sh

PROGRAM_COMMAND="qutebrowser"
PROGRAM_NAME=$(echo "$PROGRAM_COMMAND" | sed -r 's/(^|_)([a-z])/\U\2/g')

DOTFILES_PATH=$(pwd)/$PROGRAM_NAME/* 
CONFIG_PATH=~/.config/$PROGRAM_COMMAND/

echo "-------------> Installing $PROGRAM_NAME"
$pminstall $PROGRAM_COMMAND

echo "-------------> Configurando $PROGRAM"
mkdir --parent $CONFIG_PATH
ln --symbolic --force $DOTFILES_PATH $CONFIG_PATH
