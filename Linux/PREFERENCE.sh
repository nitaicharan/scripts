source config.sh
echo "-------------> Installing"
# $pminstall picom

echo "-------------> Configurando Profile"
# Configurando profile
ln -sf $(pwd)/Linux/profile ~/.profile
source config.sh


echo "-------------> Configurando Xresources"
ln --symbolic --force $(pwd)/Linux/Xresources /home/$USER/.Xresources
xrdb ~/.Xresources 

echo "-------------> Configurando Xinit"
ln -sf $(pwd)/Linux/xinitrc  ~/.xinitrc

echo "-------------> Configurando XDG Util"
ln -sf $(pwd)/Linux/mineapps.list ~/.config/

#echo "-------------> Configurando Xautolock"
# Configurando ~/.config
#pkill xautolock
#(xautolock -detectsleep -time 3 -locker "i3lock -c 000000" -notify 30 -notifier "notify-send -- 'LOCKING screen in 30 seconds'" &) &> /dev/null
