#!/bin/bash

# https://download.eclipse.org/jdtls/milestones/

source config.sh

echo "-------------> Configurando SpaceVim"
$rmrf ~/.SpaceVim.d/

curl -sLf https://spacevim.org/install.sh | bash -s -- --uninstall

curl -sLf https://spacevim.org/install.sh | bash
$cprf SpaceVim/SpaceVim.d/ ~/.SpaceVim.d/
$cprf SpaceVim/jdt-language-server ~/.SpaceVim.d/
