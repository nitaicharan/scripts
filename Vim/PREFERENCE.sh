#!/bin/bash

source config.sh

echo "-------------> Configurando Vim"
$mkdirp ~/.vim
$cprf Vim/* ~/.vim/

$cprf $(pwd)/Vim/vimrc ~/.vimrc

# Instalando plugins usando Vundle
sudo $rmrf ~/.vim/bundle/*
git clone --no-hardlinks https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim 

$pminstall cmake go python3 python3-dev build-essential 

$chown $USER:$USER ~/.vim/ -R

vim -c 'PluginInstall' -c 'exit' -c 'exit'
python ~/.vim/bundle/YouCompleteMe/install.py --all
