source config.sh

echo "-------------> Installing Tmux"
#$pminstall tmux

echo "-------------> Configurando Tmux"
mkdir --parent ~/.config/tmux
ln -sf $(pwd)/Tmux/tmux.conf ~/.tmux.conf
ln -sf $(pwd)/Tmux/tmux.conf ~/.config/tmux/keymaps.sh
