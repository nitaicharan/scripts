source config.sh

PROGRAM="dunst"
PROGRAM_NAME=$(echo "$PROGRAM" | sed -r 's/(^|_)([a-z])/\U\2/g')
CONFIG_FOLDER=$PROGRAM

echo "-------------> Installing $PROGRAM_NAME"
#$pminstall $PROGRAM

echo "-------------> Configurando $PROGRAM"
mkdir --parent ~/.config/$CONFIG_FOLDER
ln --symbolic --force $(pwd)/$PROGRAM_NAME/* ~/.config/$CONFIG_FOLDER/
