require("personal.keymaps")
require("personal.options")
require("personal.packer")
require("personal.lsp")

local home_dir = os.getenv("HOME")
package.path = home_dir .. "/.config/nvim/after/plugin/?.lua;" .. package.path
