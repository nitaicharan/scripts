local function status_line()
  local mode = "%-5{%v:lua.string.upper(v:lua.vim.fn.mode())%}"
  local file_name = "%-.16t"
  local buf_nr = "[%n]"
  local modified = " %-m"
  local file_type = " %y"
  local right_align = "%="
  local line_no = "%10([%l/%L%)]"
  local pct_thru_file = "%5p%%"

  return string.format(
    "%s%s%s%s%s%s%s%s",
    mode,
    file_name,
    buf_nr,
    modified,
    file_type,
    right_align,
    line_no,
    pct_thru_file
  )
end

-- function Gitbranch()
-- 	local result = vim.fn.trim(vim.fn.system("git -C " .. vim.fn.expand("%:p") .. " branch --show-current 2>/dev/null"))
-- 	return result
-- end
--
-- local group = vim.api.nvim_create_augroup("GitBranch", { clear = true })
--
-- vim.api.nvim_create_autocmd("BufEnter", {
-- 	group = group,
-- 	pattern = "*",
-- 	callback = function()
-- 		vim.api.nvim_buf_set_var(0, "git_branch", Gitbranch())
-- 	end,
-- })
