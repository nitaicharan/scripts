local opts = { noremap = true, silent = true }

vim.g.mapleader = " "
vim.g.maplocalleader = " "

local keymap = vim.api.nvim_set_keymap
keymap("n", "<Space>", "", opts)

keymap("i", "<ESC>", "", opts)
keymap("n", "<ESC>", "", opts)
keymap("x", "<ESC>", "", opts)

keymap("i", "<C-c>", "<ESC>", opts)
keymap("n", "<C-c>", "<ESC>", opts)
keymap("x", "<C-c>", "<ESC>", opts)
keymap("v", "<C-c>", "<ESC>", opts)

keymap("n", "<C-p>", "", opts)
keymap("n", "<C-n>", "", opts)
keymap("i", "<C-p>", "", opts)
keymap("i", "<C-n>", "", opts)

keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-l>", "<C-w>l", opts)

keymap("n", "<C-Up>", ":resize -2<CR>", opts)
keymap("n", "<C-Down>", ":resize +2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)
