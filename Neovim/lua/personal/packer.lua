local fn = vim.fn

--Automatically install packer
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
---@diagnostic disable-next-line: missing-parameter
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system({
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  })
  print("Installing packer close and reopen Neovim...")
  vim.cmd([[packadd packer.nvim]])
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd([[
augroup packer_user_config
autocmd!
autocmd BufWritePost packer.lua source <afile> | PackerSync
augroup end
]])

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  return
end
if not status_ok then
  return
end

-- Have packer use a popup window
packer.init({
  -- snapshot = "july-24",
  snapshot_path = fn.stdpath("config") .. "/snapshots",
  max_jobs = 50,
  display = {
    open_fn = function()
      return require("packer.util").float({ border = "rounded" })
    end,
    prompt_border = "rounded", -- Border style of prompt popups.
  },
})

return packer.startup(function(use)
  use("wbthomason/packer.nvim")
  -- Core
  use({ "neovim/nvim-lspconfig" })
  use({ "rafamadriz/friendly-snippets" })
  use({
    "hrsh7th/nvim-cmp",
    requires = {
      "hrsh7th/cmp-cmdline",
      "hrsh7th/cmp-path",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-nvim-lsp",
      "saadparwaiz1/cmp_luasnip",
      "L3MON4D3/LuaSnip",
    },
  })
  use({
    "WhoIsSethDaniel/mason-tool-installer.nvim",
    requires = { "williamboman/mason.nvim" },
  })
  use({
    "L3MON4D3/LuaSnip",
    tag = "v<CurrentMajor>.*",
    run = "make install_jsregexp",
  })

  use("nvim-lua/plenary.nvim")
  use("nvim-tree/nvim-web-devicons")
  use("nvim-lua/popup.nvim")
  use("JohnnyMorganz/StyLua")
  use({
    "nvim-telescope/telescope-fzf-native.nvim",
    run = "make",
    config = function()
      require("telescope").load_extension("fzf")
    end,
  })

  use("nvim-tree/nvim-tree.lua")
  use("ahmedkhalf/project.nvim")
  use("jose-elias-alvarez/null-ls.nvim")
  use("folke/which-key.nvim")
  use("nvim-telescope/telescope.nvim")
  use("nvim-treesitter/nvim-treesitter")
  use("lukas-reineke/indent-blankline.nvim")
  use("folke/neodev.nvim")
  use("goolord/alpha-nvim")
  use("EdenEast/nightfox.nvim")
  use("dvdsk/prosesitter.nvim")
  use("nvim-telescope/telescope-file-browser.nvim")
  -- use({
  -- 	"nvim-lualine/lualine.nvim",
  -- 	config = function()
  -- 		local lualine = require("lualine").setup()
  -- 	end,
  -- })
  use({
    "rcarriga/nvim-dap-ui",
    requires = { "mfussenegger/nvim-dap" },
    config = function()
      require("dapui").setup()
    end,
  })
  use({
    "TimUntersberger/neogit",
    requires = {
      "nvim-lua/plenary.nvim",
      "sindrets/diffview.nvim",
    },
  })

  -- use({
  -- 	"SmiteshP/nvim-navic",
  -- 	requires = "neovim/nvim-lspconfig",
  -- })
  use({
    "akinsho/bufferline.nvim",
    tag = "v3.*",
    requires = "nvim-tree/nvim-web-devicons",
  })
  use("psliwka/vim-smoothie")
  use({
    "akinsho/toggleterm.nvim",
    tag = "*",
    config = function()
      require("toggleterm").setup()
    end,
  })

  use({
    "kylechui/nvim-surround",
    tag = "*", -- Use for stability; omit to use `main` branch for the latest features
    config = function()
      require("nvim-surround").setup()
    end,
  })

  use({
    "numToStr/Comment.nvim",
    config = function()
      require("Comment").setup()
    end,
  })

  -- Lua
  use({
    "folke/todo-comments.nvim",
    requires = "nvim-lua/plenary.nvim",
    config = function()
      require("todo-comments").setup()
    end,
  })

  -- Lua
  use({
    "folke/trouble.nvim",
    requires = "nvim-tree/nvim-web-devicons",
    config = function()
      require("trouble").setup()
    end,
  })

  use({
    "lewis6991/gitsigns.nvim",
    config = function()
      require("gitsigns").setup()
    end,
  })

  use({
    "f-person/git-blame.nvim",
    event = "BufRead",
    config = function()
      vim.cmd("highlight default link gitblame SpecialComment")
      vim.g.gitblame_enabled = 0
    end,
  })

  use({ "mbbill/undotree" })

  use({ "ThePrimeagen/vim-be-good" })

  -- Opt

  if PACKER_BOOTSTRAP then
    require("packer").sync()
  end
end)
