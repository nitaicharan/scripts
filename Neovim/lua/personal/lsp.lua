local diagnostic_sign_error = "DiagnosticSignError"
vim.fn.sign_define(diagnostic_sign_error, {
	text = "",
	texthl = diagnostic_sign_error,
	numhl = diagnostic_sign_error,
})

local diagnostic_sign_warn = "DiagnosticSignWarn"
vim.fn.sign_define(diagnostic_sign_warn, {
	text = "",
	texthl = diagnostic_sign_warn,
	numhl = diagnostic_sign_warn,
})

local diagnostic_sign_hint = "DiagnosticSignHint"
vim.fn.sign_define(diagnostic_sign_hint, {
	text = "",
	texthl = diagnostic_sign_hint,
	numhl = diagnostic_sign_hint,
})

local diagnostic_sign_info = "DiagnosticSignInfo"
vim.fn.sign_define(diagnostic_sign_info, {
	text = "",
	texthl = diagnostic_sign_info,
	numhl = diagnostic_sign_info,
})
