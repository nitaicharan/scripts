require("neogit").setup({
  integrations = {
    diffview = true,
  },
  mappings = {
    status = {
          ["<C-c>"] = "Close",
          ["q"] = "Close",
          ["z"] = "StashPopup",
          ["D"] = "DiffPopup",
          ["d"] = "DiffAtFile",
          ["<tab>"] = "Toggle",
          ["F"] = "PullPopup",
          ["?"] = "HelpPopup",
          ["h"] = "HelpPopup",
          ["s"] = "Stage",
      -- ["S"] = "StageUnstaged",
          ["S"] = "StageAll",
          ["u"] = "Unstage",
          ["U"] = "UnstageStaged",
          ["_"] = "Discard",
          ["g"] = "RefreshBuffer",
      -- ["Q"] = "Console",
          ["Q"] = "CommandHistory",
          ["c"] = "CommitPopup",
          ["p"] = "PushPopup",
          ["P"] = "PushPopup",
          ["r"] = "RebasePopup",
          ["L"] = "LogPopup",
          ["l"] = "LogPopup",
          ["<enter>"] = "GoToFile",
          ["b"] = "BranchPopup",
          ["1"] = "Depth1",
          ["2"] = "Depth2",
          ["3"] = "Depth3",
          ["4"] = "Depth4",
          ["<c-v>"] = "VSplitOpen",
          ["<c-x>"] = "SplitOpen",
          ["<c-t>"] = "TabOpen",
    },
  },
})

vim.api.nvim_create_autocmd("FileType", {
  group = vim.api.nvim_create_augroup("buffer_mappings_neogit", { clear = true }),
  pattern = {
    "Neogit*",
  },
  callback = function()
    vim.keymap.set("n", "<C-c>", "<cmd>close<cr>", { buffer = true })
    vim.opt_local.buflisted = false
  end,
})
