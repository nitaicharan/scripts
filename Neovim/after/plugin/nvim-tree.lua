require("nvim-tree").setup({
  sync_root_with_cwd = true,
  respect_buf_cwd = true,
  view = {
    mappings = {
      custom_only = true,
      list = {
        { key = "d",           action = "remove" },
        { key = "h",           action = "parent_node" },
        { key = "H",           action = "close_node" },
        { key = "R",           action = "rename" },
        { key = "m",           action = "bulk_move" },
        { key = "p",           action = "paste" },
        { key = "P",           action = "preview" },
        { key = "cf",          action = "create" },
        { key = "gr",          action = "refresh" },
        { key = "oh",          action = "split" },
        { key = "ov",          action = "vsplit" },
        { key = "ox",          action = "system_open" },
        { key = "yf",          action = "copy" },
        { key = "ya",          action = "copy_absolute_path" },
        { key = "yp",          action = "copy_name" },
        { key = "yr",          action = "copy_path" },
        { key = { "oo", "l" }, action = "edit" },
        { key = "?",           action = "toggle_help" },
      },
    },
  },
  renderer = {
    highlight_git = true,
    root_folder_label = ":t",
    icons = {
      show = {
        git = false,
      },
    },
  },
  update_focused_file = {
    enable = true,
    update_root = true,
    debounce_delay = 15,
  },
  diagnostics = {
    enable = true,
    show_on_open_dirs = true,
  },
  git = {
    ignore = false,
    timeout = 200,
  },
  trash = {
    cmd = "trash",
  },
})
