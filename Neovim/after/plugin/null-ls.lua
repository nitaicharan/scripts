local null_ls = require("null-ls")

-- FIX: lsp and null-ls is formatting document in the same time
null_ls.setup({
  debug = true,
  sources = {
    null_ls.builtins.formatting.prettier,
    null_ls.builtins.completion.luasnip,
  },
})
