local alpha = require("alpha")
local config = require("alpha.themes.dashboard").config
alpha.setup(config)
