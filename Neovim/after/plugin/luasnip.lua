local luasnip = require("luasnip")
local packagers_tool_installer = require("packagers-tool-installer")

local loader = require("luasnip.loaders.from_vscode")
loader.lazy_load()
-- loader.lazy_load({
-- 	paths = {
-- 		"~/.local/share/nvim/site/pack/packer/start/friendly-snippets/snippets/",
-- 	},
-- })
--
-- paths[#paths + 1] = utils.join_paths(get_runtime_dir(), "site", "pack", "lazy", "opt", "friendly-snippets")
-- ~/local/share/nvim/site/pack/packer/start/friendly-snippets/snippets/
