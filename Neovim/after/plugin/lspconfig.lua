local lspconfig = require("lspconfig")
local packagers_tool_installer = require("packagers-tool-installer")

local on_attach = function(client, bufnr)
  vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

  local bufopts = { noremap = true, silent = true, buffer = bufnr }
  vim.keymap.set("n", "gD", function()
    vim.api.nvim_command("vsplit")
    vim.lsp.buf.definition()
  end, bufopts)
  vim.keymap.set("n", "gd", vim.lsp.buf.definition, bufopts)
  vim.keymap.set("n", "gI", vim.lsp.buf.implementation, bufopts)
  vim.keymap.set("n", "K", vim.lsp.buf.hover, bufopts)
  -- vim.keymap.set("n", "<C-k>", vim.lsp.buf.references, bufopts)
  -- vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, bufopts)
  -- vim.keymap.set("n", "<C-k>", vim.lsp.buf.add_workspace_folder, bufopts)
  -- vim.keymap.set("n", "<C-k>", vim.lsp.buf.remove_workspace_folder, bufopts)
  -- vim.keymap.set("n", "<C-k>", function()
  --   print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  -- end, bufopts)
  -- vim.keymap.set("n", "<C-k>", vim.lsp.buf.type_definition, bufopts)
  -- vim.keymap.set("n", "<C-k>", vim.lsp.buf.rename, bufopts)
  -- vim.keymap.set("n", "<C-k>", vim.lsp.buf.code_action, bufopts)
  vim.keymap.set("n", "<C-k>", function()
    vim.lsp.buf.format({ async = true })
  end, bufopts)
end
local lsp_flags = {
  debounce_text_changes = 150,
}

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)

for _, server in pairs(packagers_tool_installer.language_servers) do
  lspconfig[server].setup({
    on_attach = on_attach,
    flags = lsp_flags,
    capabilities = capabilities,
  })
end

vim.api.nvim_create_autocmd("FileType", {
  group = vim.api.nvim_create_augroup("buffer_mappings_lspconfig", { clear = true }),
  pattern = {
    "help",
    "qf",
  },
  callback = function()
    vim.keymap.set("n", "<C-c>", "<cmd>close<cr>", { buffer = true })
    vim.opt_local.buflisted = false
  end,
})
