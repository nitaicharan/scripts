local mason_tool_installer = require("mason-tool-installer")

local packagers_metadata = {
  { lsp = "",         mti = "angular-language-server" },
  { lsp = "tsserver", mti = "typescript-language-server" },
  { lsp = "lua_ls",   mti = "lua-language-server" },
  { lsp = "",         mti = "cspell" },
  { lsp = "",         mti = "stylua" },
  { lsp = "pyright",  mti = "pyright" },
  { lsp = "",         mti = "misspell" },
  { lsp = "",         mti = "codespell" },
  { lsp = "",         mti = "prettier" },
}

local function extractor(packers, key)
  local values = {}

  for _, value in ipairs(packers) do
    table.insert(values, value[key])
  end

  local function not_empity_string(value)
    return value ~= nil and string.len(value) > 0
  end
  local wanted = vim.tbl_filter(not_empity_string, values)

  return wanted
end

local packagers = extractor(packagers_metadata, "mti")
local language_servers = extractor(packagers_metadata, "lsp")

mason_tool_installer.setup({
  ensure_installed = packagers,
  auto_update = false,
  run_on_start = true,
  start_delay = 3000, -- 3 second delay
  debounce_hours = 5, -- at least 5 hours between attempts to install/update
})

M = {}
M.packagers = packagers
M.language_servers = language_servers

return M
