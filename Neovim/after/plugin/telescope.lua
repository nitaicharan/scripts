local telescope = require("telescope")

telescope.setup({
	defaults = {
		path_display = { "smart" },
		file_ignore_patterns = {
			".git/",
			"node_modules/*",
		},
		mappings = {
			i = {
				["<C-n>"] = false,
				["<C-p>"] = false,
				["<DOWN>"] = "cycle_history_next",
				["<UP>"] = "cycle_history_prev",
				["<C-j>"] = "move_selection_next",
				["<C-k>"] = "move_selection_previous",
				["<CR>"] = "select_default",
				["<esc>"] = false,
				["<C-c>"] = "close",
			},
			n = {
				["<C-p>"] = false,
				["<C-n>"] = false,
				["<DOWN>"] = "cycle_history_next",
				["<UP>"] = "cycle_history_prev",
				["<C-j>"] = "move_selection_next",
				["<C-k>"] = "move_selection_previous",
				["<CR>"] = "select_default",
				["<esc>"] = false,
				["<C-c>"] = "close",
				["<leader>"] = "which_key",
			},
		},
	},
	pickers = {
		colorscheme = {
			enable_preview = true,
		},
	},
	extensions = {
		project = {
			sync_with_nvim_tree = true,
		},
		media_files = {
			filetypes = { "png", "webp", "jpg", "jpeg" },
			find_cmd = "rg", -- find command (defaults to `fd`)
		},
	},
})

telescope.load_extension("file_browser")
