local which_key = require("which-key")
which_key.setup({
  plugins = {
    marks = true,       -- shows a list of your marks on ' and `
    registers = true,   -- shows your registers on " in NORMAL or <C-r> in INSERT mode
    spelling = {
      enabled = true,   -- enabling this will show WhichKey when pressing z= to select spelling suggestions
      suggestions = 20, -- how many suggestions should be shown in the list?
    },
    -- the presets plugin, adds help for a bunch of default keybindings in Neovim
    -- No actual key bindings are created
    presets = {
      operators = false,    -- adds help for operators like d, y, ... and registers them for motion / text object completion
      motions = false,      -- adds help for motions
      text_objects = false, -- help for text objects triggered after entering an operator
      windows = true,       -- default bindings on <c-w>
      nav = true,           -- misc bindings to work with windows
      z = true,             -- bindings for folds, spelling and others prefixed with z
      g = true,             -- bindings for prefixed with g
    },
  },
  -- add operators that will trigger motion and text object completion
  -- to enable all native operators, set the preset / operators plugin above
  -- operators = { gc = "Comments" },
  key_labels = {
    -- override the label used to display some keys. It doesn't effect WK in any other way.
    -- For example:
    -- ["<space>"] = "SPC",
    ["<leader>"] = "SPC",
    -- ["<cr>"] = "RET",
    -- ["<tab>"] = "TAB",
  },
  icons = {
    breadcrumb = "»", -- symbol used in the command line area that shows your active key combo
    separator = "➜", -- symbol used between a key and it's label
    group = "+",      -- symbol prepended to a group
  },
  popup_mappings = {
    scroll_down = "<c-d>", -- binding to scroll down inside the popup
    scroll_up = "<c-u>",   -- binding to scroll up inside the popup
  },
  window = {
    border = "rounded",       -- none, single, double, shadow
    position = "bottom",      -- bottom, top
    margin = { 1, 0, 1, 0 },  -- extra window margin [top, right, bottom, left]
    padding = { 2, 2, 2, 2 }, -- extra window padding [top, right, bottom, left]
    winblend = 0,
  },
  layout = {
    height = { min = 4, max = 25 },                                             -- min and max height of the columns
    width = { min = 20, max = 50 },                                             -- min and max width of the columns
    spacing = 3,                                                                -- spacing between columns
    align = "center",                                                           -- align columns left, center or right
  },
  ignore_missing = true,                                                        -- enable this to hide mappings for which you didn't specify a label
  hidden = { "<silent>", "<cmd>", "<Cmd>", "<CR>", "call", "lua", "^:", "^ " }, -- hide mapping boilerplate
  show_help = false,                                                            -- show help message on the command line when the popup is visible
  -- triggers = "auto", -- automatically setup triggers
  -- triggers = {"<leader>"} -- or specify a list manually
  triggers_blacklist = {
    -- list of mode / prefixes that should never be hooked by WhichKey
    -- this is mostly relevant for key maps that start with a native binding
    -- most people should not need to change this
    i = { "j", "k" },
    v = { "j", "k" },
  },
})

local opts = {
  mode = "n",     -- NORMAL mode
  prefix = "<leader>",
  buffer = nil,   -- Global mappings. Specify a buffer number for buffer local mappings
  silent = true,  -- use `silent` when creating keymaps
  noremap = true, -- use `noremap` when creating keymaps
  nowait = true,  -- use `nowait` when creating keymaps
}

local mappings = {
  ["/"] = { require("Comment.api").toggle.linewise.current, "Comment toggle current line" },
  --FIX terminal closing on clean
  ["<Tab>"] = { "<cmd>b#<cr>", "Previous buffer" },
  ["'"] = { "<cmd>ToggleTerm dir=%:p:h<cr>", "Terminal" },
  a = {
    name = "Applications",
    o = {
      name = "Org",
      t = { "<cmd>TodoTrouble<cr>", "Todo List" },
    },
  },
  b = {
    name = "Buffer",
    ["<C-d>"] = { "<cmd>%bdelete|edit#|bdelete#<cr>", "Kill others" },
    ["."] = {
      name = "Select buffer",

      ["."] = { "<cmd>BufferLinePick<cr>", "Pick buffer" },
      ["d"] = { "<cmd>BufferLinePickClose<cr>", "Pick delete buffer" },
    },
    b = { "<cmd>Telescope buffers<cr>", "Find" },
    d = { "<cmd>bdelete<cr>", "Close" },
    h = { "<cmd>Alpha<cr>", "Home" },
    n = { "<cmd>BufferLineCycleNext<cr>", "Next" },
    p = { "<cmd>BufferLineCyclePrev<cr>", "Previous" },
    -- FIX after reopen buffer it doesn't show in the tabs list
    u = { "<cmd>buffer#<cr>", "Reopen" },
    U = { "<cmd>Telescope oldfiles<cr>", "Recent opened" },
    i = { "<cmd>Telescope lsp_document_symbols<cr>", "Document Symbols" },
  },
  -- https//github.com/syl20bnr/spacemacs/tree/develop/layers/%2Btools/dap#key-bindings
  d = {
    name = "Debug",
    b = {
      name = "Breakpoints",
      b = { require("dap").toggle_breakpoint, "Toggle" },
    },
    d = {
      name = "Debugging",
      A = { require("dap").close, "Quit" },
      a = { require("dap").disconnect, "Disconnect" },
      d = { require("dap").continue, "Start" },
      c = { require("dap").continue, "Continue" },
      i = { require("dap").step_into, "Step Into" },
      o = { require("dap").step_out, "Step Out" },
      s = { require("dap").step_over, "Next step" },
      r = { require("dap").step_back, "Restart frame" },
    },
    w = {
      name = "Windows",
      s = { require("dap").session, "Get Session" },
      w = { require("dapui").toggle, "Debug windows" },
    },
    C = { require("dap").run_to_cursor, "Run To Cursor" },
    p = { require("dap").pause, "Pause" },
    R = { require("dap").repl.toggle, "Toggle Repl" },
  },
  e = {
    name = "Errors",
    l = { "<cmd>Telescope diagnostics bufnr=0<cr>", "Buffer Diagnostics" },
    n = { vim.diagnostic.goto_next, "Next Diagnostic" },
    p = { vim.diagnostic.goto_prev, "Prev Diagnostic" },
  },
  f = {
    name = "Files",
    f = {
      function()
        local buffer_dir = vim.fn.expand("%:p:h")

        local find_files = require("telescope.builtin").find_files
        find_files({ cwd = buffer_dir })
      end,
      "Find",
    },
    r = { "<cmd>Telescope frecency workspace=CWD<cr>", "Find" },
    s = { "<cmd>write!<cr>", "Save" },
    S = { "<cmd>wall!<cr>", "Save" },
    t = {
      function()
        require("nvim-tree.api").tree.toggle({ focus = true, find_file = true })
      end,
      "Tree",
    },
    -- TODO look for a way to confirm the deletion
    D = { "<cmd>call delete(expand('%')) | bdelete!<cr>", "Delete" },
    e = {
      name = "Config",
      d = { "<cmd>edit $MYVIMRC<cr>", "Config file" },
      R = { "<cmd>source $MYVIMRC<cr>", "Reaload" },
      --u = { "<cmd>LvimUpdate<cr>", "Update LunarVim" },
      U = { "<cmd>PackerUpdate<cr>", "Update packages" },
    },
  },
  g = {
    name = "Git",
    s = { "<cmd>Neogit<cr>", "Neogit status" },
    --g = { require 'lvim.core.terminal'.lazygit_toggle, "Lazygit" },
    b = { require("gitsigns").blame_line, "Blame" },
  },
  h = {
    name = "Help",
    d = {
      name = "Describe",
      -- TODO find in spacemacs its appropriate keymap
      k = { "<cmd>Telescope keymaps<cr>", "View LunarVim's keymappings" },
    },
  },
  j = {
    name = "Jump",
    j = { "<cmd>HopChar2<cr>", "Timer" },
    l = { "<cmd>HopLine<cr>", "Line" },
  },
  m = {
    name = "Major",
    ["="] = {
      name = "Format",

      ["="] = { vim.lsp.buf.format, "Format" },

      -- TODO: look for how make `organizeImpots` dynamically
      o = {
        "<cmd>lua vim.lsp.buf.execute_command({ command = '_typescript.organizeImports', arguments = { vim.fn.expand('%p') } })<cr>",
        "Organaze Imports",
      },
    },
    a = { vim.lsp.buf.code_action, "Code Action" },
    g = {
      name = "Goto",
      d = { vim.lsp.buf.definition, "Goto Definition" }, --TODO: not working with lua
      i = { vim.lsp.buf.implementation, "Goto Implementation" },
      r = { vim.lsp.buf.references, "References" },
    },
    r = {
      name = "Refact",
      r = { vim.lsp.buf.rename, "Rename" },
      q = { vim.diagnostic.setloclist, "Quickfix" },
    },
    h = {
      name = "Help",
      h = { vim.lsp.buf.hover, "Show hover" },
      H = { vim.lsp.buf.signature_help, "show signature help" },
    },
  },
  p = {
    name = "Projects",
    ["'"] = {
      function()
        local Terminal = require("toggleterm.terminal").Terminal
        local bottom = Terminal:new({
          dir = "git_dir",
        })
        bottom:toggle()
      end,
      "Terminal",
    },
    --P = { require("lvim.core.telescope.custom-finders").find_project_files, "Find File" },
    f = { "<cmd>Telescope find_files<cr>", "Find file" },
    p = { "<cmd>Telescope projects<cr>", "Find projects" },
    -- p = { require'telescope'.extensions.project.project{}<cr>, "Find" },
    t = {
      function()
        local nvim_tree = require("nvim-tree.api")
        nvim_tree.tree.toggle({ focus = true, find_file = true })
        nvim_tree.tree.collapse_all()
      end,
      "Tree",
    },
    E = { "<cmd>Telescope diagnostics<cr>", "Diagnostics" },
  },
  q = {
    name = "Quit",
    q = { ":confirm qall<cr>", "Kill" },
    Q = { ":qall!<cr>", "Kill" },
    s = { "<cmd>wqall<cr>", "Save and kill" },
  },
  s = {
    name = "Search",
    s = {
      function()
        -- local builtin = require("telescope.builtin")
        -- builtin.live_grep()
      end,
      "Text",
    },
    p = {
      function()
        local builtin = require("telescope.builtin")
        builtin.live_grep()
      end,
      "Workspace Symbols",
    },
    P = {
      function()
        local builtin = require("telescope.builtin")
        builtin.grep_string()
      end,
      "Search project w/ input",
    },
    G = {
      name = 'RipGrep',
      p = {
        function()
          local success, input = pcall(vim.fn.input, "Search for: ")

          if not success then
            return
          end

          local builtin = require("telescope.builtin")
          builtin.grep_string({ search = input })
        end,
        "Input Buffer project w/ input",
      },
    },
  },
  w = {
    name = "Windows",
    d = { "<cmd>quit<cr>", "Delete" },
  },
  z = {
    name = "LSP",
    a = { vim.lsp.buf.code_action, "Code Action" },
    d = { "<cmd>Telescope diagnostics bufnr=0 theme=get_ivy<cr>", "Buffer Diagnostics" },
    w = { "<cmd>Telescope diagnostics<cr>", "Diagnostics" },
    --f = { require("lvim.lsp.utils").format, "Format" },
    i = { "<cmd>LspInfo<cr>", "Info" },
    I = { "<cmd>Mason<cr>", "Mason Info" },
    j = { vim.diagnostic.goto_next, "Next Diagnostic" },
    k = { vim.diagnostic.goto_prev, "Prev Diagnostic" },
    l = { vim.lsp.codelens.run, "CodeLens Action" },
    q = { vim.diagnostic.setloclist, "Quickfix" },
    r = { vim.lsp.buf.rename, "Rename" },
    s = { "<cmd>Telescope lsp_document_symbols<cr>", "Document Symbols" },
    S = {
      "<cmd>Telescope lsp_dynamic_workspace_symbols<cr>",
      "Workspace Symbols",
    },
    e = { "<cmd>Telescope quickfix<cr>", "Telescope Quickfix" },
  },
}

local vopts = {
  mode = "v",     -- VISUAL mode
  prefix = "<leader>",
  buffer = nil,   -- Global mappings. Specify a buffer number for buffer local mappings
  silent = true,  -- use `silent` when creating keymaps
  noremap = true, -- use `noremap` when creating keymaps
  nowait = true,  -- use `nowait` when creating keymaps
}

local vmappings = {
  ["/"] = {
    ":lua require('Comment.api').toggle.linewise(vim.fn.visualmode())<CR>",
    "Comment toggle linewise (visual)",
  },
  s = { "<esc><cmd>'<,'>SnipRun<cr>", "Run range" },
  -- z = { "<cmd>TZNarrow<cr>", "Narrow" },
}

which_key.register(mappings, opts)
which_key.register(vmappings, vopts)
