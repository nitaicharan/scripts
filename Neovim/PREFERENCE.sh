source config.sh

PROGRAM="Neovim"
CONFIG_FOLDER="nvim"

echo "-------------> Installing $PROGRAM"
wget https://github.com/neovim/neovim/releases/download/stable/nvim-linux64.deb
sudo dpkg -i nvim*.deb
rm nvim*.deb
$pminstall ghostscript

echo "-------------> Configurando $PROGRAM"
mkdir --parent ~/.config/$CONFIG_FOLDER/

ln --symbolic --force $(pwd)/$PROGRAM/* /home/$USER/.config/$CONFIG_FOLDER/
nvim --headless -c 'autocmd User PackerComplete quitall' -c 'PackerSync'
