#!/bin/bash

source config.sh

PROGRAM="Alacritty"
CONFIG_FOLDER="alacritty"

echo "-------------> Configurando $PROGRAM"
mkdir --parent ~/.config/$CONFIG_FOLDER

ln --symbolic --force $(pwd)/$PROGRAM/* ~/.config/$CONFIG_FOLDER/
