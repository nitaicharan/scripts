lvim.builtin.alpha.active = true
lvim.format_on_save = false
lvim.colorscheme = "carbonfox"
lvim.builtin.terminal.active = true
lvim.builtin.nvimtree.setup.renderer.icons.show.git = false
vim.opt.relativenumber = true
lvim.builtin.nvimtree.setup.actions.open_file.quit_on_open = false
lvim.transparent_window = false
