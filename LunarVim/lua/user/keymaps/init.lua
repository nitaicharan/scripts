require("user.keymaps.telescope")
require("user.keymaps.which-key")
require("user.keymaps.nvimtree")

lvim.leader = "space"

lvim.keys = {
  normal_mode = {
    ["<C-Up>"] = ":resize +2<CR>",
    ["<C-Down>"] = ":resize -2<CR>",
    ["<C-Left>"] = ":vertical resize -2<CR>",
    ["<C-Right>"] = ":vertical resize +2<CR>",
  },

  insert_mode = {
    ["jk"] = "<Esc>";
  },

  visual_mode = {
    ["p"] = [["_dP]]
  }
}
