local _, actions = pcall(require, "telescope.actions")
lvim.builtin.telescope.defaults = {
  mappings = {
    i = {
      ["<C-j>"] = actions.move_selection_next,
      ["<C-k>"] = actions.move_selection_previous,
      ["<C-c>"] = actions.close,
    },

    n = {
      ["<Space>"] = actions.which_key,
      ["<C-j>"] = actions.move_selection_next,
      ["<C-k>"] = actions.move_selection_previous,
      ["<C-c>"] = actions.close,
      ["ov"] = actions.file_vsplit,
      ["oh"] = actions.file_split,
    },
  },
  path_display = { "smart" },
  history = {
    path = '~/.local/share/nvim/databases/telescope_history.sqlite3',
    limit = 100,
  }
}
