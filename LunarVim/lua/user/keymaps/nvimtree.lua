lvim.builtin.nvimtree.setup.view.mappings.list = {
  { key = "h", action = "close_node" },
  { key = "l", action = "open_node" },
  { key = "r", action = "rename" },
  { key = "d", action = "demove" },
  { key = "p", action = "paste" },

  { key = "dd", action = "remove" },

  { key = "ov", action = "vsplit" },
  { key = "oh", action = "split" },
  { key = "oo", action = "edit", mode = "n" },

  { key = "yy", action = "copy" },
  { key = "Vy", action = "copy_absolute_path" },
  { key = "yiw", action = "copy_name" },
  { key = "y$", action = "copy_path" },
  { key = "Y", action = "copy_path" },
}
