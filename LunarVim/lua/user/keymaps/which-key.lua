lvim.builtin.which_key.setup.popup_mappings = {
  scroll_down = "<C-j>", -- binding to scroll down inside the popup
  scroll_up = "<C-k>",   -- binding to scroll up inside the popup
}

lvim.builtin.which_key.mappings = {
  --FIX: terminal closing on clean
  ["<Tab>"] = { "<cmd>b#<cr>", "Previous buffer" },
  ["'"] = { "<cmd>ToggleTerm<cr>", "Terminal" },
  a = {
    name = "+Applications",
    o = {
      name = "+Org",
      t = { "<cmd>TodoTrouble<cr>", "Todo List" },
    },
  },
  b = {
    name = "+Buffer",
    ["<C-d>"] = { "<cmd>%bdelete|edit#|bdelete#<cr>", "Kill others" },
    ["."] = {
      name = "+Select buffer",
      ["."] = { "<cmd>BufferLinePick<cr>", "Pick buffer" },
      ["d"] = { "<cmd>BufferLinePickClose<cr>", "Pick delete buffer" }
    },
    b = { "<cmd>Telescope buffers<cr>", "Find" },
    d = { "<cmd>BufferKill<cr>", "Close" },
    h = { "<cmd>Alpha<cr>", "Home" },
    n = { "<cmd>BufferLineCycleNext<cr>", "Next" },
    p = { "<cmd>BufferLineCyclePrev<cr>", "Previous" },
    -- FIX: after reopen buffer it doesn't show in the tabs list
    u = { "<cmd>buffer#<cr>", "Reopen" },
    U = { "<cmd>Telescope oldfiles<cr>", "Recent opened" },
    i = { "<cmd>Telescope lsp_document_symbols<cr>", "Document Symbols" },
  },
  -- https://github.com/syl20bnr/spacemacs/tree/develop/layers/%2Btools/dap#key-bindings
  -- d = {
  --   name = "+Debug",

  --   b = {
  --     name = "+Breakpoints",
  --     b = { require('dap').toggle_breakpoint, "Toggle" },
  --   },


  --   d = {
  --     name = "+Debugging",
  --     -- t = { require('dap').toggle_breakpoint, "Toggle Breakpoint" },
  --     -- b = { require('dap').step_back, "Step Back" },
  --     -- c = { require('dap').continue, "Continue" },
  --     -- C = { require('dap').run_to_cursor, "Run To Cursor" },
  --     -- d = { require('dap').disconnect, "Disconnect" },
  --     -- g = { require('dap').session, "Get Session" },
  --     -- i = { require('dap').step_into, "Step Into" },
  --     -- o = { require('dap').step_over, "Step Over" },
  --     -- u = { require('dap').step_out, "Step Out" },
  --     -- p = { require('dap').pause, "Pause" },
  --     -- r = { require('dap').repl.toggle, "Toggle Repl" },
  --     -- s = { require('dap').continue, "Start" },
  --     -- q = { require('dap').close, "Quit" },
  --     -- U = { "<cmd>lua require('dapui').toggle({reset = true})<cr>", "Toggle UI" },

  --     A = { require('dap').close, "Quit" },
  --     a = { require('dap').disconnect, "Disconnect" },
  --     d = { require('dap').continue, "Start" },
  --     c = { require('dap').continue, "Continue" },
  --     i = { require('dap').step_into, "Step Into" },
  --     o = { require('dap').step_out, "Step Out" },
  --     s = { require('dap').step_over, "Next step" },
  --     r = { require('dap').step_back, "Restart frame" },
  --   },

  --   w = {
  --     name = "+Windows",
  --     s = { require('dap').session, "Get Session" },
  --     w = { require('dapui').toggle, "Debug windows" },
  --   },

  --   C = { require('dap').run_to_cursor, "Run To Cursor" },
  --   p = { require('dap').pause, "Pause" },
  --   R = { require('dap').repl.toggle, "Toggle Repl" },
  -- },

  e = {
    name = "+Errors",
    l = { "<cmd>Telescope diagnostics bufnr=0<cr>", "Buffer Diagnostics" },
    n = { vim.diagnostic.goto_next, "Next Diagnostic", },
    p = { vim.diagnostic.goto_prev, "Prev Diagnostic", },
  },
  f = {
    name = "+Files",
    f = { "<cmd>Telescope find_files cwd=%:p:h<cr>", "Find" },
    r = { "<cmd>Telescope frecency workspace=CWD<cr>", "Find" },
    s = { "<cmd>write!<cr>", "Save" },
    S = { "<cmd>wall!<cr>", "Save" },
    t = { "<cmd>NvimTreeToggle<cr>", "Tree" },
    -- TODO: look for a way to confirm the deletion
    D = { "<cmd>call delete(expand('%')) | bdelete!<cr>", "Delete" },
    e = {
      name = "+LunarVim",
      d = { "<cmd>edit " .. require("lvim.config"):get_user_config_path() .. " <cr>", "Config file" },
      R = { "<cmd>LvimReload<cr>", "Reaload" },
      u = { "<cmd>LvimUpdate<cr>", "Update LunarVim" },
      U = { "<cmd>PackerUpdate<cr>", "Update packages" },
      -- i = { "<cmd>Lazy install<cr>", "Install" },
      -- s = { "<cmd>Lazy sync<cr>", "Sync" },
      -- S = { "<cmd>Lazy clear<cr>", "Status" },
      -- c = { "<cmd>Lazy clean<cr>", "Clean" },
      -- u = { "<cmd>Lazy update<cr>", "Update" },
      -- p = { "<cmd>Lazy profile<cr>", "Profile" },
      -- l = { "<cmd>Lazy log<cr>", "Log" },
      -- d = { "<cmd>Lazy debug<cr>", "Debug" },
    },
  },
  g = {
    name = "+Git",
    s = { "<cmd>Neogit<cr>", "Neogit status" },
    g = { require('lvim.core.terminal').lazygit_toggle, "Lazygit" },
    b = { require('gitsigns').blame_line, "Blame" },
    m = {
      name = "+Magit dispatch",
      -- g = { require('lvim.core.terminal').lazygit_toggle, "Lazygit" },
      -- j = { require('gitsigns').next_hunk, "Next Hunk" },
      -- k = { require('gitsigns').prev_hunk, "Prev Hunk" },
      -- l = { require('gitsigns').blame_line, "Blame" },
      -- p = { require('gitsigns').preview_hunk, "Preview Hunk" },
      -- r = { require('gitsigns').reset_hunk, "Reset Hunk" },
      -- R = { require('gitsigns').reset_buffer, "Reset Buffer" },
      -- s = { require('gitsigns').stage_hunk, "Stage Hunk" },
      -- u = { require('gitsigns').undo_stage_hunk, "Undo Stage Hunk" },
      -- o = { "<cmd>Telescope git_status<cr>", "Open changed file" },
      -- b = { "<cmd>Telescope git_branches<cr>", "Checkout branch" },
      -- c = { "<cmd>Telescope git_commits<cr>", "Checkout commit" },
      -- C = { "<cmd>Telescope git_bcommits<cr>", "Checkout commit(for current file)" },
      -- d = { "<cmd>Gitsigns diffthis HEAD<cr>", "Git Diff" },
    },
  },
  h = {
    name = "+Help",
    d = {
      name = "+Describe",
      -- TODO find in spacemacs its appropriate keymap
      k = { "<cmd>Telescope keymaps<cr>", "View LunarVim's keymappings" },
    }
  },
  j = {
    name = "+Jump",
    j = { "<cmd>HopChar2<cr>", "Timer" },
    l = { "<cmd>HopLine<cr>", "Line" },
  },
  m = {
    name = "+Major",
    ["="] = {
      name = "+Format",

      ["="] = { require("lvim.lsp.utils").format, "Format" },
      o = {
        "<cmd>lua vim.lsp.buf.execute_command({ command = '_typescript.organizeImports', arguments = { vim.fn.expand('%:p') } })<cr>",
        "Organaze Imports", },
    },
    a = { vim.lsp.buf.code_action, "Code Action" },
    g = {
      name = "+Goto",
      d = { vim.lsp.buf.definition, "Goto Definition" },
      i = { vim.lsp.buf.implementation, "Goto Implementation" },
      r = { vim.lsp.buf.references, "References" },
    },
    r = {
      name = "+Refact",
      r = { vim.lsp.buf.rename, "Rename" },
      q = { vim.diagnostic.setloclist, "Quickfix" },
    },
    h = {
      name = "+Help",
      h = { vim.lsp.buf.hover, "Show hover" },
      H = { vim.lsp.buf.signature_help, "show signature help" },
    }
  },
  p = {
    name = "+Projects",
    ["'"] = { "<cmd>ToggleTerm dir=@%<cr>", "Terminal" },
    P = { require("lvim.core.telescope.custom-finders").find_project_files, "Find File" },
    f = { "<cmd>Telescope find_files<cr>", "Find file" },
    p = { "<cmd>Telescope projects<cr>", "Find projects" },
    -- p = { "<cmd>lua require('telescope').extensions.project.project{}<cr>", "Find" },
    t = { "<cmd>NvimTreeToggle<cr>", "Tree" },
    E = { "<cmd>Telescope diagnostics<cr>", "Diagnostics" },
  },
  q = {
    name = "+Quit",
    -- TODO: look for way to restart LunarVim with keybind <leader>qR
    q = { "<cmd>confirm qall<cr>", "Prompt kill" },
    Q = { "<cmd>qall!<cr>", "Kill LunarVim" },
    s = { "<cmd>wqall<cr>", "Save and kill" },
  },
  s = {
    name = "+Search",
    s = { "<cmd>Telescope live_grep<cr>", "Text" },
    S = {
      function()
        local telescope = require('telescope.builtin');
        local text = vim.fn.expand('<cword>');
        telescope.current_buffer_fuzzy_find({ default_text = text });
      end,
      'Search buffer w/ input'
    },
    p = { "<cmd>Telescope lsp_dynamic_workspace_symbols<cr>", "Workspace Symbols" },
    P = {
      function()
        local text = vim.fn.expand('<cword>');
        local telescope = require('telescope.builtin');
        telescope.lsp_dynamic_workspace_symbols({ default_text = text });
      end,
      'Search project w/ input'
    },
  },
  w = {
    name = "+Windows",
    d = { "<cmd>quit<cr>", "Delete" }
  },
  z = {
    name = "Test",
    a = { vim.lsp.buf.code_action, "Code Action" },
    f = { require("lvim.lsp.utils").format, "Format" },
    i = { "<cmd>LspInfo<cr>", "Info" },
    I = { "<cmd>Mason<cr>", "Mason Info" },
    d = { "<cmd>Telescope diagnostics bufnr=0 theme=get_ivy<cr>", "Buffer Diagnostics" },
    w = { "<cmd>Telescope diagnostics<cr>", "Diagnostics" },
    j = { vim.diagnostic.goto_next, "Next Diagnostic", },
    k = { vim.diagnostic.goto_prev, "Prev Diagnostic", },
    S = { "<cmd>Telescope lsp_dynamic_workspace_symbols<cr>", "Workspace Symbols", },
    l = { vim.lsp.codelens.run, "CodeLens Action" },
    q = { vim.diagnostic.setloclist, "Quickfix" },
    r = { vim.lsp.buf.rename, "Rename" },
    s = { "<cmd>Telescope lsp_document_symbols<cr>", "Document Symbols" },
    e = { "<cmd>Telescope quickfix<cr>", 'eee' },
  },
}
