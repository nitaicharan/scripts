local status_ok, markdown_preview = pcall(require, "markdown-preview")

vim.g.mkdp_auto_start = 1
