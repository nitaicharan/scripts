lvim.builtin.telescope.extensions = {
  fzf = {
    fuzzy = true, -- false will only do exact matching
    override_generic_sorter = true, -- override the generic sorter
    override_file_sorter = true, -- override the file sorter
    case_mode = "smart_case", -- or "ignore_case" or "respect_case"
  },
  project = {
    base_dirs = {
      { '~/Git', max_depth = 3 },
    },
    hidden_files = true, -- default: false
    order_by = "asc"
  },
}


lvim.builtin.telescope.on_config_done = function(telescope)
  pcall(telescope.load_extension, "frecency")
  pcall(telescope.load_extension, "project")
  pcall(telescope.load_extension, "file_browser")
  pcall(telescope.load_extension, "smart_history")
  pcall(telescope.load_extension, "live_grep_args")
end
