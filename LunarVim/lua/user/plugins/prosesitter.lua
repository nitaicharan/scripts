local prosesitter_status_ok, prosesitter = pcall(require, "prosesitter")
if not prosesitter_status_ok then
  return
end

prosesitter.setup()

local telescope_status_ok, telescope = pcall(require, "telescope")
if not telescope_status_ok then
  return
end

telescope.load_extension("prosesitter")
