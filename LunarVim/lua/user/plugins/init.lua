require "user.plugins.treesitter"
require "user.plugins.telescope"
require "user.plugins.null-ls"
require "user.plugins.mason-lspconfig"
require "user.plugins.nvim-dap"
require "user.plugins.chatgpt"
-- require("user.plugins.prosesitter")

lvim.plugins = {
  { "jose-elias-alvarez/typescript.nvim" },
  { "mxsdev/nvim-dap-vscode-js" },
  { "Everblush/everblush.nvim" },
  { "EdenEast/nightfox.nvim" },
  { "nvim-telescope/telescope.nvim" },
  { "folke/trouble.nvim" },
  { "plenary.nvim" },
  { "MunifTanjim/nui.nvim" },
  { "nvim-lua/plenary.nvim" },
  { "nvim-telescope/telescope.nvim" },
  {
    "folke/trouble.nvim",
    cmd = "TroubleToggle",
  },
  {
    "tpope/vim-surround",
    init = function()
      vim.o.timeoutlen = 500
    end,
  },

  {
    "folke/todo-comments.nvim",
    event = "BufRead",
    config = function()
      require("todo-comments").setup()
    end
  },

  {
    "sindrets/diffview.nvim",
    event = "BufRead",
  },

  { "TimUntersberger/neogit" },

  { "voldikss/vim-floaterm" },

  {
    "norcalli/nvim-colorizer.lua",
    config = function()
      require "colorizer".setup()
    end
  },

  {
    "phaazon/hop.nvim",
    event = "BufRead",
    config = function()
      require("hop").setup()
    end,
  },

  {
    "ethanholz/nvim-lastplace",
    event = "BufRead",
    config = function()
      require("nvim-lastplace").setup()
    end,
  },

  { "tpope/vim-repeat" },

  {
    "f-person/git-blame.nvim",
    event = "BufRead",
    config = function()
      vim.cmd "highlight default link gitblame SpecialComment"
      vim.g.gitblame_enabled = 0
    end,
  },

  {
    "npxbr/glow.nvim",
    ft = { "markdown" }
  },

  {
    "ellisonleao/carbon-now.nvim",
    config = function()
      require('carbon-now').setup()
    end
  },

  {
    "jinh0/eyeliner.nvim",
    config = function()
      require("eyeliner").setup {
        highlight_on_key = true,
      }
    end,
  },

  -- install without yarn or npm
  {
    "iamcco/markdown-preview.nvim",
    build = function()
      vim.fn["mkdp#util#install"]()
    end,
  },
  {
    "iamcco/markdown-preview.nvim",
    build = "cd app && npm install",
    init = function() vim.g.mkdp_filetypes = { "markdown" } end,
    ft = { "markdown" },
  },

  { "jackMort/ChatGPT.nvim" },
  { "github/copilot.vim" }


  -- TODO: add a grammar check plugin
  -- {
  --   'dvdsk/prosesitter',
  -- }

  -- TODO: take look at friendly-snippets
  -- TODO: take look at folke/persistence.nvimf

  --[[
     TODO: Add `SonarLint` plugin and make a `ftplugin` standalone configuration
      Regardless of project SonarLint should runs without need install plugins
      and addicinal project configuration on its .eslintrc
      https://github.com/SonarSource/eslint-plugin-sonarjs
   ]]

  -- { "nvim-treesitter/nvim-treesitter-angular" }

  -- {
  --   'wfxr/minimap.vim',
  --   require = { "wfxr/code-minimap" },
  --   run = "cargo install --locked code-minimap",
  --   config = function()
  --     vim.cmd("let g:minimap_width = 10")
  --     vim.cmd("let g:minimap_auto_start = 1")
  --     vim.cmd("let g:minimap_auto_start_win_enter = 1")
  --   end,
  -- },
}
