lvim.lsp.installer.setup.ensure_installed = {
  "angularls",
  "dockerls",
  "cssls",
  "html",
  "jsonls",
  "angularls",
  "tsserver",
  "lua_ls",
  "yamlls",
  "vimls",
  "tailwindcss",
  "sqlls",
}
