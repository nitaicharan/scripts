local formatters = require "lvim.lsp.null-ls.formatters"
local linters = require "lvim.lsp.null-ls.linters"

linters.setup({
  { command = "codespell", },
  { command = "ruff", },
  { command = "eslint", },
})

formatters.setup({
  { command = "eslint", },
  { command = 'prettier' },
  { command = 'prettierd' },
  { command = 'black' },
  { command = "ruff", },
})
