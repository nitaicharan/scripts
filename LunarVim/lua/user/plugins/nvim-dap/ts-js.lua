-- Setup lsp.
vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "tsserver" })

local typescript_status_oky, typescript = pcall(require, "typescript")
if not typescript_status_oky then
  print("typescript package has not been fould!")
  return
end

local dap_vscode_js_status_oky, dap_vscode_js = pcall(require, "dap-vscode-js")
if not dap_vscode_js_status_oky then
  print("dap-vscode-js package has not been fould!")
  return
end

local dap_status_oky, dap = pcall(require, "dap")
if not dap_status_oky then
  print("dap package has not been fould!")
  return
end

local lsp = require("lvim.lsp")

typescript.setup {
  -- disable_commands = false, -- prevent the plugin from creating Vim commands
  debug = false, -- enable debug logging for commands
  go_to_source_definition = {
    fallback = true, -- fall back to standard LSP definition on failure
  },
  server = { -- pass options to lspconfig's setup method
    on_attach = lsp.common_on_attach,
    on_init = lsp.common_on_init,
    capabilities = lsp.common_capabilities(),
  },
}

local mason_path = vim.fn.glob(vim.fn.stdpath "data" .. "/mason/")
dap_vscode_js.setup {
  -- node_path = "node", -- Path of node executable. Defaults to $NODE_PATH, and then "node"
  debugger_path = mason_path .. "packages/js-debug-adapter", -- Path to vscode-js-debug installation.
  -- debugger_cmd = { "js-debug-adapter" }, -- Command to use to launch the debug server. Takes precedence over `node_path` and `debugger_path`.
  adapters = { "pwa-node", "pwa-chrome", "pwa-msedge", "node-terminal", "pwa-extensionHost" }, -- which adapters to register in nvim-dap
}

for _, language in ipairs { "typescript", "javascript" } do
  dap.configurations[language] = {
    {
      type = "pwa-node",
      request = "launch",
      name = "Launch file",
      program = "${file}",
      cwd = "${workspaceFolder}",
    },
    {
      type = "pwa-node",
      request = "attach",
      name = "Attach",
      processId = require("dap.utils").pick_process,
      cwd = "${workspaceFolder}",
    }
  }
end

-- Set a linter.
-- local linters = require("lvim.lsp.null-ls.linters")
-- linters.setup({
--   { command = "eslint", filetypes = { "javascript", "typescript" } },
-- })
