source config.sh

PROGRAM="LunarVim"
CONFIG_FOLDER="lvim"

echo "-------------> Uninstalling $PROGRAM"
bash <(curl -s https://raw.githubusercontent.com/lunarvim/lunarvim/master/utils/installer/uninstall.sh)

echo "-------------> Installing $PROGRAM"
# wget https://github.com/neovim/neovim/releases/download/stable/nvim-linux64.deb
# sudo dpkg -i nvim*.deb
# rm nvim*.deb

bash <(curl -s https://raw.githubusercontent.com/lunarvim/lunarvim/master/utils/installer/install.sh)
echo "-------------> Configurando $PROGRAM"
mkdir --parent ~/.config/$CONFIG_FOLDER/

ln --symbolic --force $(pwd)/$PROGRAM/* /home/$USER/.config/$CONFIG_FOLDER/
