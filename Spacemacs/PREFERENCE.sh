source config.sh

DIR_SOURCE=/tmp
DIR_DESTINY=~/.emacs.d/

echo "-------------> Install Emacs"
#sudo rm -f /etc/apt/preferences.d/nosnap.pref
#$pminstall snapd
#sudo snap install emacs --classic

echo "-------------> Install Spacemacs"
wget -O $DIR_SOURCE/Spacemacs.zip https://github.com/syl20bnr/spacemacs/archive/develop.zip
unzip -o $DIR_SOURCE/Spacemacs.zip -d $DIR_SOURCE
rm -rf $DIR_DESTINY
mv -f $DIR_SOURCE/spacemacs-develop  $DIR_DESTINY


echo "-------------> Configurando Spacemacs"
ln -sf $(pwd)/Spacemacs/spacemacs /home/$USER/.spacemacs
