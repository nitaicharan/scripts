source config.sh

echo "-------------> Installing Range"
$pminstall ranger

echo "-------------> Configurando Range"

rm -rf ~/.config/ranger/
mkdir -p ~/.config/ranger/
mkdir -p ~/.config/ranger/plugins

git clone https://github.com/alexanderjeurissen/ranger_devicons ~/.config/ranger/plugins/ranger_devicons

ln -sf $(pwd)/Ranger/* ~/.config/ranger/
sudo chmod u+x ~/.config/ranger/scope.sh -R
