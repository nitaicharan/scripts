source config.sh

echo "-------------> Installing Newsboat"
$pminstall newsboat

echo "-------------> Configurando Newsboat"
rm -rf ~/.newsboat
mkdir -p ~/.newsboat
ln -sf $(pwd)/Newsboat/* /home/$USER/.newsboat
