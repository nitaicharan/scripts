#!/bin/bash
source config.sh

PROGRAM="Rofi"
CONFIG_FOLDER="rofi"

echo "-------------> Configurando $PROGRAM"
mkdir --parent ~/.config/$CONFIG_FOLDER

ln --symbolic --force $(pwd)/$PROGRAM/* ~/.config/$CONFIG_FOLDER/
