source config.sh

PROGRAM_COMMAND="nb"
#PROGRAM_NAME=$(echo "$PROGRAM_COMMAND" | sed -r 's/(^|_)([a-z])/\U\2/g')
PROGRAM_NAME=NB

DOTFILES_PATH=$(pwd)/$PROGRAM_NAME/* 
#CONFIG_PATH=~/.config/$PROGRAM_COMMAND/
CONFIG_PATH=~/.$PROGRAM_COMMAND/

echo "-------------> Installing $PROGRAM_NAME"
# $pminstall git

echo "-------------> Configurando $PROGRAM"
# mkdir --parent $CONFIG_PATH
# ln --symbolic --force $DOTFILES_PATH $CONFIG_PATH

rm --recursive --force $CONFIG_PATH

sudo curl -L https://raw.github.com/xwmx/nb/master/nb -o /usr/local/bin/nb &&
sudo chmod +x /usr/local/bin/nb &&
nb completions install --download

git clone git@github.com:nitaicharan/Personal-Articles.git $CONFIG_PATH/home
