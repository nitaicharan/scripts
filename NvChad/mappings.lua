local M = {}

M.lspconfig = {
  n = {
    [";=="] = {
      function()
        vim.lsp.buf.format {}
      end,
      "lsp formatting",
    },
  },
}

M.telescope = {
  n = {
    ["<leader>ff"] = { "<cmd> Telescope find_files <CR>" },
    ["<leader>fa"] = { "<cmd> Telescope find_files follow=true no_ignore=true hidden=true <cr>", "find all" },
    ["<leader>fw"] = { "<cmd> Telescope live_grep <cr>", "live grep" },
    ["<leader>fb"] = { "<cmd> Telescope buffers <cr>", "find buffers" },
    ["<leader>fh"] = { "<cmd> Telescope help_tags <cr>", "help page" },
    ["<leader>fo"] = { "<cmd> Telescope oldfiles <cr>", "find oldfiles" },
    ["<leader>tk"] = { "<cmd> Telescope keymaps <cr>", "show keys" },
  },
}


-- more keybinds!

return M
