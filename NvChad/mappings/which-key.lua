local{ M = {}

M.which_key = {
  n = {
    ["leader"] = {
      name = "test",

      a = {
        name = "+Applications",

        o = {
          name = "+Org",

          t = { "<cmd>:TodoTrouble<cr>", "Todo List" },
        },
      },

      b = {
        name = "+Buffer",

        ["<C-d>"] = { "<cmd>:BufferLineCloseLeft<cr>:BufferLineCloseRight<cr>", "Kill others" },
        ["."] = {
          name = "+Select buffer",


          ["."] = { "<cmd>:BufferLinePick<cr>", "Pick buffer" },
          ["d"] = { "<cmd>:BufferLinePickClose<cr>", "Pick delete buffer" }
        },

        b = { "<cmd>:Telescope buffers<cr>", "Find" },
        d = { "<cmd>:BufferKill<cr>", "Close" },
        h = { "<cmd>:Alpha<cr>", "Home" },
        n = { "<cmd>:BufferLineCycleNext<cr>", "Next" },
        p = { "<cmd>:BufferLineCyclePrev<cr>", "Previous" },
        -- FIX: after reopen buffer it doesn't show in the tabs list
        u = { "<cmd>:buffer#<cr>", "Reopen" },
        U = { "<cmd>:Telescope oldfiles<cr>", "Recent opened" },
        i = { "<cmd>:Telescope lsp_document_symbols<cr>", "Document Symbols" },
      },

      -- https://github.com/syl20bnr/spacemacs/tree/develop/layers/%2Btools/dap#key-bindings
      d = {
        name = "+Debug",

        b = {
          name = "+Breakpoints",
          b = { "<cmd>lua require'dap'.toggle_breakpoint()<cr>", "Toggle" },
        },


        d = {
          name = "+Debugging",

          A = { "<cmd>lua require'dap'.close()<cr>", "Quit" },
          a = { "<cmd>lua require'dap'.disconnect()<cr>", "Disconnect" },
          d = { "<cmd>lua require'dap'.continue()<cr>", "Start" },
          c = { "<cmd>lua require'dap'.continue()<cr>", "Continue" },
          i = { "<cmd>lua require'dap'.step_into()<cr>", "Step Into" },
          o = { "<cmd>lua require'dap'.step_out()<cr>", "Step Out" },
          s = { "<cmd>lua require'dap'.step_over()<cr>", "Next step" },
          r = { "<cmd>lua require'dap'.step_back()<cr>", "Restart frame" },
        },

        w = {
          name = "+Windows",
          s = { "<cmd>lua require'dap'.session()<cr>", "Get Session" },
          w = { "<cmd>lua require'dapui'.toggle()<cr>", "Debug windows" },
        },

        C = { "<cmd>lua require'dap'.run_to_cursor()<cr>", "Run To Cursor" },
        p = { "<cmd>lua require'dap'.pause()<cr>", "Pause" },
        R = { "<cmd>lua require'dap'.repl.toggle()<cr>", "Toggle Repl" },
      },

      e = {
        name = "+Errors",

        l = { "<cmd>:Telescope diagnostics bufnr=0<cr>", "Buffer Diagnostics" },
        n = { vim.diagnostic.goto_next, "Next Diagnostic", },
        p = { vim.diagnostic.goto_prev, "Prev Diagnostic", },
      },

      f = {
        name = "+Files",
        f = { "<cmd>:Telescope find_files cwd=%:p:h<cr>", "Find" },
        r = { "<cmd>:Telescope frecency workspace=CWD<cr>", "Find" },
        s = { "<cmd>:write!<CR>", "Save" },
        S = { "<cmd>:wall!<CR>", "Save" },
        t = { "<cmd>:NvimTreeToggle<cr>", "Tree" },
        -- TODO: look for a way to confirm the deletion
        D = { "<cmd>:call delete(expand('%')) | bdelete!<cr>", "Delete" },

        l = {
          name = "+LunarVim",
          d = { "<cmd>:edit " .. require("lvim.config"):get_user_config_path() .. " <CR>", "Config file" },
          R = { "<cmd>:LvimReload<cr>", "Reaload" },
          u = { "<cmd>:LvimUpdate<cr>", "Update LunarVim" },
          U = { "<cmd>:PackerUpdate<cr>", "Update packages" },
        },

      },

      g = {
        name = "+Git",

        s = { "<cmd>:Neogit<cr>", "Neogit status" },
        g = { "<cmd>:lua require 'lvim.core.terminal'.lazygit_toggle()<cr>", "Lazygit" },
        b = { "<cmd>:lua require 'gitsigns'.blame_line()<cr>", "Blame" },
      },

      h = {
        name = "+Help",

        d = {
          name = "+Describe",

          -- TODO find in spacemacs its appropriate keymap
          k = { "<cmd>:Telescope keymaps<cr>", "View LunarVim's keymappings" },
        }
      },

      j = {
        name = "+Jump",

        j = { "<cmd>:HopChar2<cr>", "Timer" },
        l = { "<cmd>:HopLine<cr>", "Line" },
      },

      m = {
        name = "+Major",

        ["="] = {
          name = "+Format",

          ["="] = { require("lvim.lsp.utils").format, "Format", },
          o = { "<cmd>:lua vim.lsp.buf.execute_command({ command = '_typescript.organizeImports', arguments = { vim.fn.expand('%:p') } })<cr>",
            "Organaze Imports", },
        },
        a = { "<cmd>:lua vim.lsp.buf.code_action()<cr>", "Code Action" },
        g = {
          name = "+Goto",

          d = { "<cmd>:lua vim.lsp.buf.definition()<CR>", "Goto Definition" },
          i = { "<cmd>:lua vim.lsp.buf.implementation()<CR>", "Goto Implementation" },
          r = { "<cmd>:lua vim.lsp.buf.references()<CR>", "References" },
        },

        r = {
          name = "+Refact",

          r = { vim.lsp.buf.rename, "Rename" },
          q = { vim.diagnostic.setloclist, "Quickfix" },
        },

        h = {
          name = "+Help",
          h = { vim.lsp.buf.hover, "Show hover" },
          H = { vim.lsp.buf.signature_help, "show signature help" },
        }
      },

      p = {
        name = "+Projects",

        ["'"] = { "<cmd>:ToggleTerm dir=@%<cr>", "Terminal" },
        P = { require("lvim.core.telescope.custom-finders").find_project_files, "Find File" },
        f = { "<cmd>:Telescope find_files<cr>", "Find file" },
        p = { "<cmd>:Telescope projects<cr>", "Find projects" },
        -- p = { "<cmd>:lua require'telescope'.extensions.project.project{}<CR>", "Find" },
        t = { "<cmd>:NvimTreeToggle<cr>", "Tree" },
        E = { "<cmd>:Telescope diagnostics<cr>", "Diagnostics" },
      },

      q = {
        name = "+Quit",

        -- TODO: look for way to restart LunarVim with keybind <leader>qR
        q = { "<cmd>:lua require('lvim.utils.functions').smart_quit()<CR>", "Kill" },
        s = { "<cmd>:wqall<CR>", "Save and kill" },
      },

      s = {
        name = "+Search",
        s = { "<cmd>:Telescope live_grep<cr>", "Text" },
        p = { "<cmd>:Telescope lsp_dynamic_workspace_symbols<cr>", "Workspace Symbols" },
      },

      w = {

        name = "+Windows",

        d = { "<cmd>:quit<CR>", "Delete" }
      },

      z = {
        name = "LSP",
        a = { "<cmd>lua vim.lsp.buf.code_action()<cr>", "Code Action" },
        d = { "<cmd>Telescope diagnostics bufnr=0 theme=get_ivy<cr>", "Buffer Diagnostics" },
        w = { "<cmd>Telescope diagnostics<cr>", "Diagnostics" },
        f = { require("lvim.lsp.utils").format, "Format" },
        i = { "<cmd>LspInfo<cr>", "Info" },
        I = { "<cmd>Mason<cr>", "Mason Info" },
        j = { vim.diagnostic.goto_next, "Next Diagnostic" },
        k = { vim.diagnostic.goto_prev, "Prev Diagnostic" },
        l = { vim.lsp.codelens.run, "CodeLens Action" },
        q = { vim.diagnostic.setloclist, "Quickfix" },
        r = { vim.lsp.buf.rename, "Rename" },
        s = { "<cmd>Telescope lsp_document_symbols<cr>", "Document Symbols" },
        S = {
          "<cmd>Telescope lsp_dynamic_workspace_symbols<cr>",
          "Workspace Symbols",
        },
        e = { "<cmd>Telescope quickfix<cr>", "Telescope Quickfix" },
      },
    },


    ["<leader>fb"] = { "<cmd> telescope buffers <cr>", "find buffers" },
    ["<leader>fh"] = { "<cmd> telescope help_tags <cr>", "help page" },
    ["<leader>fo"] = { "<cmd> telescope oldfiles <cr>", "find oldfiles" },
    ["<leader>tk"] = { "<cmd> telescope keymaps <cr>", "show keys" },
  },
}

-- more keybinds!

return M
