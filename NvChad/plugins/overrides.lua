local M = {}

M.treesitter = {
  ensure_installed = {
    "typescript",
    "javascript",
    "html",
    "css",
  },
}

M.mason = {
  ensure_installed = {
    "angular-language-server",
    "css-lsp",
    "html-lsp",
    "typescript-language-server",
  },
}

-- git support in nvimtree
M.nvimtree = {
  git = {
    enable = true,
  },

  renderer = {
    highlight_git = true,
    icons = {
      show = {
        git = true,
      },
    },
  },
}

return M
