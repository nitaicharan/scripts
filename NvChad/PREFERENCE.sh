source config.sh

echo "-------------> Uninstalling NvChad"
$rmrf -rf ~/.config/nvim
$rmrf -rf ~/.local/share/nvim
$rmrf -rf ~/.cache/nvim

echo "-------------> Installing NvChad"
$pminstall neovim

$pminstall git
git clone https://github.com/NvChad/NvChad --branch=dev ~/.config/nvim --depth 1

echo "-------------> Configurando NvChad"
$mkdirp ~/.config/nvim/lua/custom
$lnsf $(pwd)/NvChad/* /home/$USER/.config/nvim/lua/custom/

