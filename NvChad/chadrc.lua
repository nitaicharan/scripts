local M = {}

M.plugins = require "custom.plugins"
M.mappings = require "custom.mappings"

M.ui = {
  theme_toggle = { "onedark", "gatekeeper" },
  theme = "gatekeeper",
}

return M
