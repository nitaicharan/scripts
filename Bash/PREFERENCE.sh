source config.sh

PROGRAM="Bash"
CONFIG_FOLDER="bash"

echo "-------------> Installing $PROGRAM"
#$pminstall bash

echo "-------------> Configurando $PROGRAM"
ln --symbolic --force $(pwd)/$PROGRAM/bashrc ~/.bashrc
ln --symbolic --force $(pwd)/$PROGRAM/bash_profile ~/.bash_profile
