source config.sh

PROGRAM="Zathura"
CONFIG_FOLDER="zathura"

echo "-------------> Installing $PROGRAM"
$pminstall zathura


echo "-------------> Configurando $PROGRAM"
mkdir --parent ~/.config/$CONFIG_FOLDER
ln --symbolic --force $(pwd)/$PROGRAM/* ~/.config/$CONFIG_FOLDER/
