#!/bin/bash

source config.sh
$pminstall conky

echo "-------------> Configurando Conky"
$mkdirp ~/.config/conky/
$cprf Conky/* ~/.config/conky/

sudo killall conky 2> /dev/null
cd "/conky/TeejeeTech"
conky -c "/home/$USER/.conky/TeejeeTech/Process Panel" &
