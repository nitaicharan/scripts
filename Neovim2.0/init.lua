local vim = vim
local home_dir = os.getenv("HOME")
package.path = home_dir .. "/.config/nvim/after/?.lua;" .. package.path

-- =====================================================================
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end

vim.opt.rtp:prepend(lazypath)
vim.g.mapleader = "<space>"
vim.g.maplocalleader = "<space>"

require("lazy").setup({
	{ "neovim/nvim-lspconfig", lazy = true },

	{ "jose-elias-alvarez/null-ls.nvim", lazy = true },

	{ "nvim-treesitter/nvim-treesitter", lazy = true },

	{ "rafamadriz/friendly-snippets", lazy = true },

	{
		"L3MON4D3/LuaSnip",
		event = "InsertEnter",
		dependencies = {
			"friendly-snippets",
		},
	},

	{
		"williamboman/mason-lspconfig.nvim",
		dependencies = {
			"williamboman/mason.nvim",
			"neovim/nvim-lspconfig",
		},
		lazy = true,
	},

	{
		"hrsh7th/nvim-cmp",
		dependencies = {
			"hrsh7th/cmp-cmdline",
			"hrsh7th/cmp-path",
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-nvim-lsp",
			"saadparwaiz1/cmp_luasnip",
			"L3MON4D3/LuaSnip",
		},
		lazy = true,
	},

	{
		"williamboman/mason.nvim",
		build = ":masonupdate",
		lazy = true,
	},

	{
		"WhoIsSethDaniel/mason-tool-installer.nvim",
		dependencies = {
			"williamboman/mason.nvim",
			"neovim/nvim-lspconfig",
		},
		lazy = true,
	},

	{
		"nvim-lua/popup.nvim",
		lazy = true,
	},

	{
		"nvim-neo-tree/neo-tree.nvim",
		branch = "v2.x",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
			"MunifTanjim/nui.nvim",
		},
		lazy = true,
	},

	{ "nvim-tree/nvim-tree.lua", lazy = true },

	{
		"nvim-tree/nvim-tree.lua",
		lazy = true,
		dependencies = {},
	},

	{ "ahmedkhalf/project.nvim", lazy = true },

	{
		"nvim-telescope/telescope.nvim",
		tag = "0.1.1",
		dependencies = { "nvim-lua/plenary.nvim" },
		lazy = true,
	},

	{
		"nvim-telescope/telescope-fzf-native.nvim",
		build = "make",
		lazy = true,
	},

	{
		"nvim-telescope/telescope-project.nvim",
		dependencies = { "nvim-telescope/telescope.nvim" },
		lazy = true,
	},

	{
		"nvim-telescope/telescope-file-browser.nvim",
		dependencies = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" },
		lazy = true,
	},

	{
		"natecraddock/workspaces.nvim",
		lazy = true,
	},

	{
		"folke/which-key.nvim",
		keys = "<space>",
		config = function()
			require("domain.use_cases.mapper").setup()
		end,
		lazy = true,
	},

	{ "lukas-reineke/indent-blankline.nvim", lazy = true },
	{ "folke/neodev.nvim", lazy = true },

	{
		"goolord/alpha-nvim",
		dependencies = { "nvim-tree/nvim-web-devicons" },
		config = function()
			-- require("alpha").setup(require("alpha.themes.startify").config)
		end,
		lazy = true,
	},

	{ "EdenEast/nightfox.nvim", priority = 1000 },

	-- ({
	-- 	"nvim-lualine/lualine.nvim",
	-- 	config = function(),
	-- 		local lualine = require("lualine"),.setup()
	-- 	end,
	-- }),
	-- {
	--   "rcarriga/nvim-dap-ui",
	--   dependencies = { "mfussenegger/nvim-dap" },
	--   config = function()
	--     require("dapui").setup()
	--   end,
	-- },

	{
		"TimUntersberger/neogit",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"sindrets/diffview.nvim",
		},
		lazy = true,
	},

	-- {
	-- 	"SmiteshP/nvim-navic",
	-- 	dependencies = "neovim/nvim-lspconfig",
	-- },

	{ "psliwka/vim-smoothie", lazy = true },

	{
		"akinsho/toggleterm.nvim",
		version = "*",
		config = true,
		lazy = true,
	},

	{
		"kylechui/nvim-surround",
		version = "*", -- Use for stability; omit to use `main` branch for the latest features
		event = "VeryLazy",
		config = function()
			require("nvim-surround").setup({
				-- Configuration here, or leave empty to use defaults
			})
		end,
		lazy = true,
	},

	{ "numToStr/Comment.nvim", lazy = true },

	{
		"folke/todo-comments.nvim",
		dependencies = "nvim-lua/plenary.nvim",
		config = function()
			require("todo-comments").setup()
		end,
		lazy = true,
	},

	{
		"folke/trouble.nvim",
		dependencies = "nvim-tree/nvim-web-devicons",
		config = function()
			require("trouble").setup()
		end,
		lazy = true,
	},

	{
		"lewis6991/gitsigns.nvim",
		config = function()
			require("gitsigns").setup()
		end,
		lazy = true,
	},

	{
		"f-person/git-blame.nvim",
		event = "BufRead",
		config = function()
			vim.cmd("highlight default link gitblame SpecialComment")
			vim.g.gitblame_enabled = 0
		end,
		lazy = true,
	},

	{ "mbbill/undotree", lazy = true },

	{
		"kristijanhusak/vim-dadbod-ui",
		dependencies = "tpope/vim-dadbod",
		cmd = "DBUI",
		lazy = true,
	},

	{
		"rest-nvim/rest.nvim",
		dependencies = "nvim-lua/plenary.nvim",
		config = function()
			require("rest-nvim").setup({})
		end,
	},
  lazy=true,
})
-- =====================================================================

require("domain")
require("options")
