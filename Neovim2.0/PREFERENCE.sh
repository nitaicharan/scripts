source config.sh

PROGRAM="Neovim2.0"
CONFIG_FOLDER="nvim"

echo "-------------> Installing $PROGRAM"
#wget https://github.com/neovim/neovim/releases/download/stable/nvim-linux64.deb
#sudo dpkg -i nvim*.deb
#rm nvim*.deb
#$pminstall ghostscript

echo "-------------> Configurando $PROGRAM"
mkdir --parent ~/.config/$CONFIG_FOLDER/

ln --symbolic --force $(pwd)/$PROGRAM/* /home/$USER/.config/$CONFIG_FOLDER/
