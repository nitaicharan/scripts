local versior_interface = require("interfaces.versior")

local function _setup()
	local status_oky, pkg = pcall(require, "gitsigns")
	if not status_oky then
		return
	end
	pkg.setup()

	local status_neogit_oky, neogit_pkg = pcall(require, "neogit")
	if not status_neogit_oky then
		return
	end

	neogit_pkg.setup()
	return {
		gitsigns = pkg,
		neogit = neogit_pkg,
	}
end

versior_interface.blame_line = function()
	local api = _setup().gitsigns
	if not api then
		return
	end

	api.blame_line()
end

versior_interface.open = function()
	local api = _setup().neogit
	if not api then
		return
	end

	api.open()
end

return versior_interface
