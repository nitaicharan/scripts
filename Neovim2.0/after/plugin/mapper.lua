local mapper_interface = require("interfaces.mapper")

mapper_interface.setup = function()
  local mapper_map = require("domain.maps.mapper")

	local which_key_use_case = require("which-key")

	which_key_use_case.setup({
		plugins = {
			marks = true,
			registers = true,
			spelling = {
				enabled = true,
				suggestions = 20,
			},
			presets = {
				operators = false,
				motions = false,
				text_objects = false,
				windows = true,
				nav = true,
				z = true,
				g = true,
			},
		},
		key_labels = {
			["<leader>"] = "SPC",
		},
		icons = {
			breadcrumb = "»",
			separator = "➜",
			group = "+",
		},
		popup_mappings = {
			scroll_down = "<c-d>",
			scroll_up = "<c-u>",
		},
		window = {
			border = "rounded",
			position = "bottom",
			margin = { 1, 0, 1, 0 },
			padding = { 2, 2, 2, 2 },
			winblend = 0,
		},
		layout = {
			height = { min = 4, max = 25 },
			width = { min = 20, max = 50 },
			spacing = 3,
			align = "center",
		},
		ignore_missing = true,
		hidden = { "<silent>", "<cmd>", "<Cmd>", "<CR>", "call", "lua", "^:", "^ " },
		show_help = false,
		triggers_blacklist = {
			i = { "j", "k" },
			v = { "j", "k" },
		},
	})

	local opts = mapper_map.normal_mode.opts
	local mappings = mapper_map.normal_mode.mappings

	local vopts = mapper_map.visual_mode.opts
	local vmappings = mapper_map.visual_mode.mappings

	which_key_use_case.register(mappings, opts)
	which_key_use_case.register(vmappings, vopts)
end

return mapper_interface
