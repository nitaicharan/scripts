local comment_interface = require("interfaces.comment")
local vim = vim

local function _setup()
	local status_oky, pkg = pcall(require, "Comment")
	if not status_oky then
		return
	end

	pkg.setup()
	return require("Comment.api").toggle
end

comment_interface.toggle_current_line_visual_mode = function()
	local api = _setup()
	if not api then
		return
	end

	local esc = vim.api.nvim_replace_termcodes("<ESC>", true, false, true)
	vim.api.nvim_feedkeys(esc, "nx", false)
	api.linewise(vim.fn.visualmode())
end

comment_interface.toggle_current_line = function()
	local api = _setup()
	if not api then
		return
	end

	api.linewise.current()
end

return comment_interface
