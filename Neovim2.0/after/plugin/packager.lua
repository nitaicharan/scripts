local packager_interface = require("interfaces.packager")

packager_interface.install = function()
  require("lazy").install()
end

packager_interface.update = function()
  require("lazy").update()
end

return packager_interface
