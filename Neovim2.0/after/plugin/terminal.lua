local terminal_interface = require("interfaces.terminal")

local function _setup(opts)
	opts = opts or {}
	local status_oky, pkg = pcall(require, "toggleterm")
	if not status_oky then
		return
	end

	pkg.setup()
	return require("toggleterm.terminal").Terminal:new(opts)
end

terminal_interface.open = function()
	local api = _setup()
	if not api then
		return
	end

	api:open()
end

terminal_interface.close = function()
	local api = _setup()
	if not api then
		return
	end

	api:close()
end

terminal_interface.toggle = function(opts)
	local api = _setup(opts)
	if not api then
		return
	end

	api:toggle()
end

return terminal_interface
