local finder_interface = require("interfaces.finder")
local vim = vim

local function _setup()
	local status_oky, pkg = pcall(require, "gitsigns")
	if not status_oky then
		return
	end

	require("plugin.telescope").setup()
	return require("telescope.builtin")
end

finder_interface.find_files = function()
	local api = _setup()
	if not api then
		return
	end

	local buffer_dir = vim.fn.expand("%:p:h")
	api.find_files({ cwd = buffer_dir })
end

finder_interface.git_files = function()
	local api = _setup()
	if not api then
		return
	end

	api.git_files()
end

finder_interface.buffers = function()
	local api = _setup()
	if not api then
		return
	end

	api.buffers()
end

finder_interface.oldfiles = function()
	local api = _setup()
	if not api then
		return
	end

	api.oldfiles()
end
finder_interface.diagnostics = function()
	local api = _setup()
	if not api then
		return
	end

	api.diagnostics()
end
finder_interface.keymaps = function()
	local api = _setup()
	if not api then
		return
	end

	api.keymaps()
end

finder_interface.live_grep = function(opts)
	local api = _setup()
	if not api then
		return
	end

	opts = opts or {}
	api.live_grep({
		search_dirs = opts.files,
	})
end

finder_interface.grep = function(opts)
	opts = opts or {}

	local api = _setup()
	if not api then
		return
	end

	api.grep_string({
		search = opts.text,
		search_dirs = opts.files,
	})
end

finder_interface.lsp_document_symbols = function()
	local api = _setup()
	if not api then
		return
	end

	api.lsp_document_symbols()
end

finder_interface.help_tags = function()
	local api = _setup()
	if not api then
		return
	end

	api.help_tags()
end

finder_interface.man_pages = function()
	local api = _setup()
	if not api then
		return
	end

	api.man_pages()
end

return finder_interface
