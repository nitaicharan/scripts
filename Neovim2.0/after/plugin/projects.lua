local projects_interface = require("interfaces.projects")

local function _setup()
	local status_ok, pkp = pcall(require, "workspaces")
	if not status_ok then
		return
	end

	pkp.setup({
		auto_open = true,
		notify_info = true,
	})

	local status_telescope_ok, telescope_plugin = pcall(require, "telescope")
	if not status_telescope_ok then
		return
	end
	require("plugin.telescope").setup({
		projects = {
			workspaces = {
				n = {
					c = false,
					-- 				{ key = "d", action = "remove" },
					-- d = actions.remove,
					-- 				{ key = "h", action = "parent_node" },
					-- h = actions.goto_parent_dir,
					-- 				{ key = "H", action = "close_node" },
					["<C-c><C-p>d"] = "delete_project",
					-- 				{ key = "m", action = "bulk_move" },
					-- 				{ key = "p", action = "paste" },
					-- 				{ key = "P", action = "preview" },
					-- cf = actions.create,
					-- 				{ key = "gr", action = "refresh" },
					-- 				{ key = "oh", action = "split" },
					-- 				{ key = "ov", action = "vsplit" },
					-- 				{ key = "ox", action = "system_open" },
					-- yf = actions.copy,
					-- 				{ key = "ya", action = "copy_absolute_path" },
					-- 				{ key = "yp", action = "copy_name" },
					-- 				{ key = "yr", action = "copy_path" },
					-- 				{ key = { "oo", "l" }, action = "edit" },
					-- 				{ key = "?", action = "toggle_help" },
					-- ox = actions.open,
				},
			},
		},
	})

	telescope_plugin.load_extension("workspaces")
	return require("telescope").extensions.workspaces
	-- return require("telescope").extensions.projects.projects({})
end

projects_interface.open = function()
	local api = _setup()
	if not api then
		return
	end

	api.workspaces()
end

return projects_interface
