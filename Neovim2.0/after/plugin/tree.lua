local tree_interface = require("interfaces.tree")
local vim = vim

local function on_attach(bufnr)
	local api = require("nvim-tree.api")

	local function opts(desc)
		return { desc = "nvim-tree: " .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
	end

	vim.keymap.set("n", "d", api.fs.remove, opts("Delete"))
	vim.keymap.set("n", "h", api.node.navigate.parent, opts("Parent Directory"))
	vim.keymap.set("n", "H", api.node.navigate.parent_close, opts("Close Directory"))
	vim.keymap.set("n", "R", api.fs.rename, opts("Rename"))
	vim.keymap.set("n", "m", api.marks.bulk.move, opts("Move Bookmarked"))
	vim.keymap.set("n", "p", api.fs.paste, opts("Paste"))
	vim.keymap.set("n", "P", api.node.open.preview, opts("Open Preview"))
	vim.keymap.set("n", "cf", api.fs.create, opts("Create"))
	vim.keymap.set("n", "gr", api.tree.reload, opts("Refresh"))
	vim.keymap.set("n", "oh", api.node.open.horizontal, opts("Open: Horizontal Split"))
	vim.keymap.set("n", "ov", api.node.open.vertical, opts("Open: Vertical Split"))
	vim.keymap.set("n", "ox", api.node.run.system, opts("Run System"))
	vim.keymap.set("n", "yf", api.fs.copy.node, opts("Copy"))
	vim.keymap.set("n", "ya", api.fs.copy.absolute_path, opts("Copy Absolute Path"))
	vim.keymap.set("n", "yp", api.fs.copy.filename, opts("Copy Name"))
	vim.keymap.set("n", "yr", api.fs.copy.relative_path, opts("Copy Relative Path"))
	vim.keymap.set("n", "oo", api.node.open.edit, opts("Open"))
	vim.keymap.set("n", "l", api.node.open.edit, opts("Open"))
	vim.keymap.set("n", "?", api.tree.toggle_help, opts("Help"))
end

local function _setup(opts)
	opts = opts or {}
	require("nvim-tree").setup({
		sync_root_with_cwd = true,
		respect_buf_cwd = true,
		on_attach = opts.on_attach,
		renderer = {
			highlight_git = true,
			root_folder_label = ":t",
			icons = {
				show = {
					git = false,
				},
			},
		},
		update_focused_file = {
			enable = true,
			update_root = true,
			debounce_delay = 15,
		},
		diagnostics = {
			enable = true,
			show_on_open_dirs = true,
		},
		git = {
			ignore = false,
			timeout = 200,
		},
		trash = {
			cmd = "trash",
		},
	})
end

_setup({ on_attach = on_attach })

local api = require("nvim-tree.api")

tree_interface.open = function()
	api.tree.open()
end

tree_interface.close = function()
	api.tree.close()
end

tree_interface.toggle = function()
	api.tree.toggle()
end

return tree_interface
