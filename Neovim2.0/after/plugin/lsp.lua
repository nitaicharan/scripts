local lsp_interface = require("interfaces.lsp")
local vim = vim

local function _setup()
	local lspconfig = require("lspconfig")
	local cmp = require("cmp")
	local luasnip = require("luasnip")

	local kind_icons = {
		Array = "",
		Boolean = "",
		Class = "",
		Color = "",
		Constant = "",
		Constructor = "",
		Enum = "",
		EnumMember = "",
		Event = "",
		Field = "",
		File = "",
		Folder = "",
		Function = "",
		Interface = "",
		Key = "",
		Keyword = "",
		Method = "",
		Module = "",
		Namespace = "",
		Null = "ﳠ",
		Number = "",
		Object = "",
		Operator = "",
		Package = "",
		Property = "",
		Reference = "",
		Snippet = "",
		String = "",
		Struct = "",
		Text = "",
		TypeParameter = "",
		Unit = "",
		Value = "",
		Variable = "",
	}

	local source_names = {
		luasnip = "(Snippet)",
		nvim_lsp = "(LSP)",
		emoji = "(Emoji)",
		path = "(Path)",
		calc = "(Calc)",
		cmp_tabnine = "(Tabnine)",
		vsnip = "(Snippet)",
		buffer = "(Buffer)",
		tmux = "(TMUX)",
		copilot = "(Copilot)",
		treesitter = "(TreeSitter)",
	}

	vim.api.nvim_create_autocmd("FileType", {
		group = vim.api.nvim_create_augroup("buffer_mappings_lspconfig", { clear = true }),
		pattern = {
			"help",
			"qf",
		},
		callback = function()
			vim.keymap.set("n", "<C-c>", "<cmd>close<cr>", { buffer = true })
			vim.opt_local.buflisted = false
		end,
	})

	local toggle_complete = function()
		return cmp.mapping({
			i = function()
				if cmp.visible() then
					return cmp.abort()
				end

				cmp.complete()
			end,
			c = function()
				if cmp.visible() then
					return cmp.close()
				end

				cmp.complete()
			end,
		})
	end

	local mapping = cmp.mapping.preset.insert({
		["<C-k>"] = cmp.mapping(cmp.mapping.select_prev_item(), { "i", "c" }),
		["<C-j>"] = cmp.mapping(cmp.mapping.select_next_item(), { "i", "c" }),
		["<C-Space>"] = toggle_complete(),
		["<C-e>"] = cmp.mapping.abort(),
		["<CR>"] = cmp.mapping.confirm({ select = true }),
		["<Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_next_item()
			elseif luasnip.expand_or_locally_jumpable() then
				luasnip.expand_or_jump()
			else
				fallback()
			end
		end, { "i", "s" }),
		["<S-Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_prev_item()
			elseif luasnip.jumpable(-1) then
				luasnip.jump(-1)
			else
				fallback()
			end
		end, { "i", "s" }),
	})

	cmp.setup({
		snippet = {
			expand = function(args)
				luasnip.lsp_expand(args.body)
			end,
		},
		experimental = {
			ghost_text = false,
			native_menu = false,
		},
		formatting = {
			fields = { "kind", "abbr", "menu" },
			max_width = 0,
			format = function(entry, vim_item)
				vim_item.kind = kind_icons[vim_item.kind]
				vim_item.menu = source_names[entry.source.name]
				return vim_item
			end,
		},
		mapping = mapping,
		sources = cmp.config.sources({
			{ name = "luasnip" },
			{
				name = "nvim_lsp",
				entry_filter = function(entry, ctx)
					local kind = require("cmp.types.lsp").CompletionItemKind[entry:get_kind()]
					if kind == "Snippet" and ctx.prev_context.filetype == "java" then
						return false
					end
					if kind == "Text" then
						return false
					end
					return true
				end,
			},
			{ name = "buffer" },
			{ name = "path" },
			{ name = "cmdline" },
			{ name = "cmp_tabnine" },
			{ name = "nvim_lua" },
			{ name = "calc" },
			{ name = "emoji" },
			{ name = "treesitter" },
			{ name = "crates" },
			{ name = "tmux" },
		}),
	})
	--
	cmp.setup.filetype("gitcommit", {
		sources = cmp.config.sources({
			{ name = "cmp_git" },
		}, {
			{ name = "buffer" },
		}),
	})

	cmp.setup.cmdline({ "/", "?" }, {
		mapping = cmp.mapping.preset.cmdline(),
		sources = {
			{ name = "buffer" },
		},
	})

	cmp.setup.cmdline(":", {
		mapping = cmp.mapping.preset.cmdline(),
		sources = cmp.config.sources({
			{ name = "path" },
		}, {
			{ name = "cmdline" },
		}),
	})

	local null_ls = require("null-ls")
	null_ls.setup({
		sources = {
			null_ls.builtins.formatting.stylua,
			null_ls.builtins.completion.spell,
			-- null_ls.builtins.formatting.prettierd,
			-- null_ls.builtins.diagnostics.eslint_d,
			-- null_ls.builtins.formatting.eslint_d,
			-- null_ls.builtins.code_actions.eslint_d,
			null_ls.builtins.diagnostics.ruff,
			null_ls.builtins.formatting.ruff,
			null_ls.builtins.formatting.black,
		},
	})

	require("nvim-treesitter.configs").setup({
		sync_install = false,
		auto_install = true,
		highlight = {
			enable = true,
		},
		additional_vim_regex_highlighting = false,
	})

	require("mason").setup()

	require("mason-lspconfig").setup({
		ensure_installed = { "lua_ls" },
	})

	require("mason-tool-installer").setup({
		auto_update = true,
		debounce_hours = 24,
	})

	local on_attach = function(client, bufnr)
		vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")
		local bufopts = { noremap = true, silent = true, buffer = bufnr }

		-- vim.keymap.set("n", "gD", function()
		--   vim.api.nvim_command("vsplit")
		--   vim.lsp.buf.definition()
		-- end, bufopts)
		vim.keymap.set("n", "gd", vim.lsp.buf.definition, bufopts)
		vim.keymap.set("n", "gI", vim.lsp.buf.implementation, bufopts)
		vim.keymap.set("n", "K", vim.lsp.buf.hover, bufopts)
		-- vim.keymap.set("n", "<C-k>", vim.lsp.buf.references, bufopts)
		-- vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, bufopts)
		-- vim.keymap.set("n", "<C-k>", vim.lsp.buf.add_workspace_folder, bufopts)
		-- vim.keymap.set("n", "<C-k>", vim.lsp.buf.remove_workspace_folder, bufopts)
		-- vim.keymap.set("n", "<C-k>", function()
		--   print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
		-- end, bufopts)
		-- vim.keymap.set("n", "<C-k>", vim.lsp.buf.type_definition, bufopts)
		-- vim.keymap.set("n", "<C-k>", vim.lsp.buf.rename, bufopts)
		-- vim.keymap.set("n", "<C-k>", vim.lsp.buf.code_action, bufopts)
	end
	local lsp_flags = {
		debounce_text_changes = 150,
	}

	local updated_capabilities = vim.lsp.protocol.make_client_capabilities()
	vim.tbl_deep_extend("force", updated_capabilities, require("cmp_nvim_lsp").default_capabilities())
	updated_capabilities.textDocument.completion.completionItem.insertReplaceSupport = false
	updated_capabilities.textDocument.codeLens = { dynamicRegistration = false }

	local language_servers = require("mason-lspconfig").get_installed_servers()
	for _, server in pairs(language_servers) do
		lspconfig[server].setup({
			on_attach = on_attach,
			flags = lsp_flags,
			capabilities = updated_capabilities,
		})
	end
end

-- require("luasnip.loaders").from_vscode.lazy_load()
lsp_interface.format = function(opt)
	_setup()
	opt = opt or {}
	vim.lsp.buf.format({
		async = opt.async or true,
		bufnr = opt.bufnr or 0,
		filter = opt.filter,
	})
end

return lsp_interface
