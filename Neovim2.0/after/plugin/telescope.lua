local M = {}

M.setup = function(opts)
  opts = opts or {}
  local actions = require("telescope.actions")
  require("telescope").setup({
    defaults = {
      default_mappings = false,
      path_display = { "smart" },
      file_ignore_patterns = {
        ".git/",
        "node_modules/*",
      },
      mappings = {
        i = {
          ["<DOWN>"] = actions.cycle_history_next,
          ["<UP>"] = actions.cycle_history_prev,
          ["<C-j>"] = actions.move_selection_next,
          ["<C-k>"] = actions.move_selection_previous,
          ["<CR>"] = actions.select_default,
        },
        n = {
          o = false,
          ["<DOWN>"] = actions.cycle_history_next,
          ["<UP>"] = actions.cycle_history_prev,
          ["<C-j>"] = actions.move_selection_next,
          ["<C-k>"] = actions.move_selection_previous,
          ["<CR>"] = actions.select_default,
          ["?"] = actions.which_key,
        },
      },
    },
    pickers = {
      colorscheme = {
        enable_preview = true,
      },
    },
    extensions = {
      project = {
        sync_with_nvim_tree = true,
      },
      media_files = {
        filetypes = { "png", "webp", "jpg", "jpeg" },
        find_cmd = "rg", -- find command (defaults to `fd`)
      },
      file_browser = opts.file_browser,
      projects = opts.projects,
    },
    mappings = {
      i = {},
      n = {},
    },
  })
end

return M
