M = {}

M.git_files = function() end

M.buffers = function() end

M.oldfiles = function() end

M.diagnostics = function() end

M.keymaps = function() end

M.live_grep = function(opts) end

M.grep = function(opts) end

M.find_files = function() end

M.lsp_document_symbols = function() end

M.help_tags = function() end

M.man_pages = function() end

return M
