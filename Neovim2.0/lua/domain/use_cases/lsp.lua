local lsp_adapter = require("plugin.lsp")

local M = {}

M.format_buffer = function()
  lsp_adapter.format()
end

return M
