local packager_adapter = require("plugin.packager")

local M = {}

M.install = function()
  packager_adapter.install()
end

M.update = function()
  packager_adapter.update()
end

return M
