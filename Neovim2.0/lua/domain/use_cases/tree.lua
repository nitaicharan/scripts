local tree_adapter = require("plugin.tree")

local M = {}

M.open = function()
  tree_adapter.open()
end

M.close = function()
  tree_adapter.close()
end

M.toggle = function()
  tree_adapter.toggle()
end

return M
