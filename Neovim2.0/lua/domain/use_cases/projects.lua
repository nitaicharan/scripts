local finder_adapter = require("plugin.projects")

local M = {}

M.open = function()
  finder_adapter.open()
end

return M
