local terminal_adapter = require("plugin.terminal")
local vim = vim

local M = {}

M.open = function()
	terminal_adapter.open()
end

M.close = function()
	terminal_adapter.close()
end

M.toggle = function()
	terminal_adapter.toggle()
end

M.toggle_buffer_dir = function()
	local buffer = vim.fn.expand("%:p:h")
	terminal_adapter.toggle({
		dir = buffer,
	})
end

return M
