local versior_adapter = require("plugin.versior")

local M = {}

M.blame_line = function()
	versior_adapter.blame_line()
end

M.open = function()
	versior_adapter.open()
end

return M
