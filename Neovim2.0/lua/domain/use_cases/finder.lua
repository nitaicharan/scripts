local finder_adapter = require("plugin.finder")
local vim = vim

local M = {}

M.git_files = function()
	finder_adapter.git_files()
end

M.buffers = function()
	finder_adapter.buffers()
end

M.oldfiles = function()
	finder_adapter.oldfiles()
end

M.lsp_document_symbols = function()
	finder_adapter.lsp_document_symbols()
end

M.keymaps = function()
	finder_adapter.keymaps()
end

M.find_files = function()
	finder_adapter.find_files()
end

M.projects = function()
	finder_adapter.projects()
end

M.diagnostics = function()
	finder_adapter.diagnostics({ bufnr = 0 })
end

M.live_grep = function()
	finder_adapter.live_grep()
end

M.grep = function(opts)
	opts = opts or {}
	finder_adapter.grep(opts)
end

M.grep_input = function()
	local success, input = pcall(vim.fn.input, "Search for: ")

	if not success then
		return
	end

	M.grep({ text = input })
end

M.grep_hover_project = function()
	local text = vim.fn.expand("<cword>")
	M.grep({ text })
end

M.grep_hover = function()
	local text = vim.fn.expand("<cword>")
	local file = vim.fn.expand("%")
	M.grep({ text, files = { file } })
end

M.live_grep_in_file = function()
	local file = vim.fn.expand("%")
	finder_adapter.live_grep({ files = { file } })
end

M.help_tags = function()
	finder_adapter.help_tags()
end

M.man_pages = function()
	finder_adapter.man_pages()
end

return M
