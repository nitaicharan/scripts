local mapper_adapter = require("plugin.mapper")

local M = {}

M.setup = function()
  mapper_adapter.setup()
end

return M
