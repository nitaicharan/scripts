local comment_adapter = require("plugin.comment")

local M = {}

M.toggle_current_line = function()
  comment_adapter.toggle_current_line()
end

M.toggle_current_line_visual_mode = function()
  comment_adapter.toggle_current_line_visual_mode()
end

return M
