local finder_use_case = require("domain.use_cases.finder")
local lsp_use_case = require("domain.use_cases.lsp")
local packager_use_case = require("domain.use_cases.packager")
local tree_use_case = require("domain.use_cases.tree")
local terminal_use_case = require("domain.use_cases.terminal")
local comment_use_case = require("domain.use_cases.comment")
local versior_use_case = require("domain.use_cases.versior")
local projects_use_case = require("domain.use_cases.projects")

local vim = vim

return {
	normal_mode = {
		opts = {
			prefix = "<leader>",
			silent = true,
			mode = "n",
			buffer = nil,
			noremap = true,
			nowait = true,
		},
		mappings = {
			[";"] = { comment_use_case.toggle_current_line, "Comment toggle current line" },
			["<Tab>"] = { "<cmd>buffer#<cr>", "Previous buffer" },
			["'"] = { terminal_use_case.toggle_buffer_dir, "Terminal" },
			a = {
				name = "Applications",
				o = {
					name = "Org",
					t = { "<cmd>TodoTrouble<cr>", "Todo List" },
				},
			},
			b = {
				name = "Buffer",
				["<C-d>"] = { ":%bdelete|edit#|bdelete#<cr>", "Kill others" },
				["."] = {
					name = "Select buffer",

					["."] = { "<cmd>BufferLinePick<cr>", "Pick buffer" },
					["d"] = { "<cmd>BufferLinePickClose<cr>", "Pick delete buffer" },
				},
				b = { finder_use_case.buffers, "Find" }, -- FIX: exception
				d = { "<cmd>bdelete<cr>", "Close" },
				h = { "<cmd>Alpha<cr>", "Home" },
				n = { ":bnext<cr>", "Next" },
				p = { ":bprevious<cr>", "Previous" },
				u = { ":buffer#<cr>", "Reopen" },
				i = { finder_use_case.lsp_document_symbols, "Document Symbols" },
			},
			e = {
				name = "Errors",
				l = { finder_use_case.diagnostics, "Buffer Diagnostics" },
				n = { vim.diagnostic.goto_next, "Next Diagnostic" },
				p = { vim.diagnostic.goto_prev, "Prev Diagnostic" },
			},
			f = {
				name = "Files",
				D = { "<cmd>call delete(expand('%')) | bdelete!<cr>", "Delete" },
				e = {
					name = "Config",
					d = { "<cmd>edit $MYVIMRC<cr>", "Config file" },
					R = { "<cmd>source $MYVIMRC<cr>", "Reaload" },
					U = { packager_use_case.update, "Update packages" },
				},
				f = { finder_use_case.find_files, "Find files" },
				r = { finder_use_case.oldfiles, "Recent opened" },
				s = { "<cmd>write!<cr>", "Save" },
				S = { "<cmd>wall!<cr>", "Save" },
				t = { tree_use_case.toggle, "Tree" },
			},
			g = {
				name = "Git",
				s = { versior_use_case.open, "Neogit status" },
				b = { versior_use_case.blame_line, "Blame" },
			},
			h = {
				name = "Help",
				d = {
					name = "Describe",
					k = { finder_use_case.keymaps, "Keymappings" },
					f = { finder_use_case.help_tags, "Functions" },
				},
				m = { finder_use_case.man_pages, "Man Pages" },
			},
			j = {
				name = "Jump",
				j = { "<cmd>HopChar2<cr>", "Timer" },
				l = { "<cmd>HopLine<cr>", "Line" },
			},
			m = {
				name = "Major",
				["="] = {
					name = "Format",

					["="] = { lsp_use_case.format_buffer, "Format" },
					o = {
						"<cmd>lua vim.lsp.buf.execute_command({ command = '_typescript.organizeImports', arguments = { vim.fn.expand('%p') } })<cr>",
						"Organaze Imports",
					},
				},
				a = { vim.lsp.buf.code_action, "Code Action" },
				g = {
					name = "Goto",
					d = { vim.lsp.buf.definition, "Goto Definition" },
					i = { vim.lsp.buf.implementation, "Goto Implementation" },
					r = { vim.lsp.buf.references, "References" },
				},
				r = {
					name = "Refact",
					r = { vim.lsp.buf.rename, "Rename" },
					q = { vim.diagnostic.setloclist, "Quickfix" },
				},
				h = {
					name = "Help",
					h = { vim.lsp.buf.hover, "Show hover" },
					H = { vim.lsp.buf.signature_help, "show signature help" },
				},
			},
			p = {
				name = "Projects",
				["'"] = { terminal_use_case.toggle, "Terminal" },
				f = { finder_use_case.git_files, "Find file" },
				p = { projects_use_case.open, "Find projects" },
				t = { tree_use_case.toggle, "Tree" },
				E = { finder_use_case.diagnostics, "Diagnostics" },
			},
			q = {
				name = "Quit",
				q = { ":confirm qall<cr>", "Kill" },
				Q = { ":qall!<cr>", "Kill" },
				s = { "<cmd>wqall<cr>", "Save and kill" },
			},
			s = {
				name = "Search",
				s = { finder_use_case.live_grep_in_file, "Text" },
				S = { finder_use_case.grep_hover, "Search w/ input" },
				p = { finder_use_case.live_grep, "Workspace Symbols" },
				P = { finder_use_case.grep_hover_project, "Search project w/ input" },
				G = {
					name = "RipGrep",
					p = { finder_use_case.grep_input, "Input Buffer project w/ input" },
				},
			},
			w = {
				name = "Windows",
				d = { "<cmd>quit<cr>", "Delete" },
			},
			z = {
				name = "LSP",
				a = { vim.lsp.buf.code_action, "Code Action" },
				i = { "<cmd>LspInfo<cr>", "Info" },
				I = { "<cmd>Mason<cr>", "Mason Info" },
				j = { vim.diagnostic.goto_next, "Next Diagnostic" },
				k = { vim.diagnostic.goto_prev, "Prev Diagnostic" },
				l = { vim.lsp.codelens.run, "CodeLens Action" },
				q = { vim.diagnostic.setloclist, "Quickfix" },
				r = { vim.lsp.buf.rename, "Rename" },
			},
		},
	},
	visual_mode = {
		opts = {
			mode = "v",
			prefix = "<leader>",
			buffer = nil,
			silent = true,
			noremap = true,
			nowait = true,
		},
		mappings = {
			["/"] = { comment_use_case.toggle_current_line_visual_mode, "Comment toggle linewise (visual)" },
			s = { "<esc><cmd>'<,'>SnipRun<cr>", "Run range" },
		},
	},
}
